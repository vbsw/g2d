# window

[![GoDoc](https://godoc.org/gitlab.com/vbsw/g2d/window?status.svg)](https://godoc.org/gitlab.com/vbsw/g2d/window) [![Stability: Experimental](https://masterminds.github.io/stability/experimental.svg)](https://masterminds.github.io/stability/experimental.html)

## About
window is a package for Go to create a window with OpenGL 3.0 context. It is published on <https://gitlab.com/vbsw/g2d/window>.

## Copyright
Copyright 2021, Vitali Baumtrok (vbsw@mailbox.org).

window is distributed under the Boost Software License, version 1.0. (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)

window is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Boost Software License for more details.

## Compile
Install Go (https://golang.org/doc/install). For Cgo install a C compiler (<https://jmeubank.github.io/tdm-gcc/>).

For Windows:
To compile an executable that doesn't open a console, use

	-ldflags -H=windowsgui

## References
- https://golang.org/doc/install
- https://jmeubank.github.io/tdm-gcc/
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- https://dave.cheney.net/2013/10/12/how-to-use-conditional-compilation-with-the-go-build-tool
- https://github.com/golang/go/wiki/cgo
- https://pkg.go.dev/cmd/go#hdr-Compile_packages_and_dependencies
- https://pkg.go.dev/cmd/link
