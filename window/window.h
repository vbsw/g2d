#ifndef G2DWINDOW_H
#define G2DWINDOW_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(G2DWINDOW_WIN32)
typedef unsigned long g2d_ul_t;
extern void g2d_window_init(int *err, g2d_ul_t *err_win32);
extern void g2d_window_new(void **data, int go_obj, int x, int y, int w, int h, int wn, int hn, int wx, int hx, int b, int d, int r, int f, int l, int c, int *err, g2d_ul_t *err_win32, char **err_str);
extern void g2d_context(void *data, void **dc, void **rc);
extern void g2d_ctx_make_current(void *dc, void *rc, int *err, g2d_ul_t *err_win32, char **err_str);
extern void g2d_ctx_release(int *err, g2d_ul_t *err_win32, char **err_str);
extern void g2d_swap_buffers(void *dc, int *err, g2d_ul_t *err_win32, char **err_str);
extern void g2d_window_show(void *const data);
extern void g2d_get_window_props(void *data, int *x, int *y, int *w, int *h, int *wn, int *hn, int *wx, int *hx, int *b, int *d, int *r, int *f, int *l);
extern void g2d_set_window_props(void *data, int x, int y, int w, int h, int wn, int hn, int wx, int hx, int b, int d, int r, int f, int l);
extern void g2d_close(void *data);
extern void g2d_post_custom(void *data);
extern void g2d_window_destroy(void *const data);
extern void g2d_free_mem(void *mem);
extern void g2d_process_events();
#elif defined(G2DWINDOW_LINUX)
#endif

#ifdef __cplusplus
}
#endif

#endif /* G2DWINDOW_H */