/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package window provides the creation of a window with OpenGL 3.0 context.
package window

// #cgo CFLAGS: -DG2DWINDOW_WIN32
// #cgo LDFLAGS: -luser32 -lgdi32 -lOpenGL32
// #include "window.h"
import "C"
import (
	"errors"
	"gitlab.com/vbsw/g2d/event"
	"gitlab.com/vbsw/g2d/queue"
	"sync"
	"unsafe"
)

// Window is the window with OpenGL context.
type Window struct {
	dataC   unsafe.Pointer
	hndlC   int
	queue   *queue.Queue
	mutex   sync.Mutex
	handler Handler
	props   Properties
	state   int
	err     error
}

// tContext provides OpenGL context functions.
type tContext struct {
	dc unsafe.Pointer
	rc unsafe.Pointer
}

// Init initializes WGL functions. An OpenGL context must have
// been made current before calling this function.
func Init() error {
	if !initialized {
		var errC C.int
		var errWin32C C.g2d_ul_t
		C.g2d_window_init(&errC, &errWin32C)
		if errC == 0 {
			initialized = true
			return nil
		}
		return toError(int(errC), uint64(errWin32C), nil)
	}
	return nil
}

// ProcessEvents retrieves messages from thread's message queue for all windows and calls window's handler to process it.
// This function blocks until further messages are available and returns only if all windows are destroyed.
func ProcessEvents() {
	if initialized {
		C.g2d_process_events()
	} else {
		panic(notInitialized)
	}
}

// New returns a new instance of Window.
func New(params *Parameters) (*Window, error) {
	if initialized {
		var errC C.int
		var errWin32C C.g2d_ul_t
		var errStrC *C.char
		params = ensureParams(params)
		window := new(Window)
		window.init(params, &errC, &errWin32C, &errStrC)
		if errC == 0 {
			window.queue = queue.New(0)
			window.handler = newWrapper(window, params.handler, params.AutoUpdate, params.Threaded)
			window.state = 1
			err := window.handler.OnInit(window.context())
			if err == nil {
				return window, nil
			}
			window.Destroy()
			return nil, err
		}
		err := toError(int(errC), uint64(errWin32C), errStrC)
		deleteHndl(window.hndlC)
		if errStrC != nil {
			C.g2d_free_mem(unsafe.Pointer(errStrC))
		}
		return nil, err
	}
	panic(notInitialized)
}

// Show sets the window visible, and starts logic thread, eventually.
func (window *Window) Show() {
	C.g2d_window_show(window.dataC)
	window.updateProps()
	propsBak := window.props
	window.err = window.handler.OnShow(&window.props)
	window.props.Destroy = bool(window.err != nil)
	if window.props != propsBak {
		window.applyProps()
	}
}

// PostEvent sends an event to window.
func (window *Window) PostEvent(ev interface{}) {
	window.mutex.Lock()
	if window.state == 0 && window.err == nil {
		if _, ok := ev.(*event.Close); ok {
			C.g2d_close(window.dataC)
		} else {
			window.queue.Put(ev)
			C.g2d_post_custom(window.dataC)
		}
	}
	window.mutex.Unlock()
}

// Destroy closes the window and releases all ressources associated with it.
// This function does not return until logic thread stops.
func (window *Window) Destroy() {
	if window.state == 1 {
		window.handler.OnDestroy()
		C.g2d_window_destroy(window.dataC)
		deleteHndl(window.hndlC)
		window.queue = nil
		window.handler = nil
	}
}

// init allocates memory and initializes the window.
func (window *Window) init(params *Parameters, errC *C.int, errWin32C *C.g2d_ul_t, errStrC **C.char) {
	x := C.int(params.ClientX)
	y := C.int(params.ClientY)
	w := C.int(params.ClientWidth)
	h := C.int(params.ClientHeight)
	wn := C.int(params.ClientMinWidth)
	hn := C.int(params.ClientMinHeight)
	wx := C.int(params.ClientMaxWidth)
	hx := C.int(params.ClientMaxHeight)
	c := toCInt(params.Centered)
	l := toCInt(params.MouseLocked)
	b := toCInt(params.Borderless)
	d := toCInt(params.Dragable)
	r := toCInt(params.Resizable)
	f := toCInt(params.Fullscreen)
	window.hndlC = registerHndl(window)
	C.g2d_window_new(&(window.dataC), C.int(window.hndlC), x, y, w, h, wn, hn, wx, hx, b, d, r, f, l, c, errC, errWin32C, errStrC)
}

// context returns the window's OpenGL context.
func (window *Window) context() Context {
	ctx := new(tContext)
	if window != nil {
		C.g2d_context(window.dataC, &ctx.dc, &ctx.rc)
	}
	return ctx
}

func (window *Window) updateProps() {
	var x, y, w, h, wn, hn C.int
	var wx, hx, b, d, r, f, l C.int
	C.g2d_get_window_props(window.dataC, &x, &y, &w, &h, &wn, &hn, &wx, &hx, &b, &d, &r, &f, &l)
	window.props.ClientX = int(x)
	window.props.ClientY = int(y)
	window.props.ClientWidth = int(w)
	window.props.ClientHeight = int(h)
	window.props.ClientMinWidth = int(wn)
	window.props.ClientMinHeight = int(hn)
	window.props.ClientMaxWidth = int(wx)
	window.props.ClientMaxHeight = int(hx)
	window.props.Borderless = bool(b != 0)
	window.props.Dragable = bool(d != 0)
	window.props.Resizable = bool(r != 0)
	window.props.Fullscreen = bool(f != 0)
	window.props.MouseLocked = bool(l != 0)
	window.props.Destroy = false
	//window.props.MouseX = xm
	//window.props.MouseY = ym
}

func (window *Window) applyProps() {
	if window.props.Destroy {
		window.Destroy()
	} else {
		x := C.int(window.props.ClientX)
		y := C.int(window.props.ClientY)
		w := C.int(window.props.ClientWidth)
		h := C.int(window.props.ClientHeight)
		wn := C.int(window.props.ClientMinWidth)
		hn := C.int(window.props.ClientMinHeight)
		wx := C.int(window.props.ClientMaxWidth)
		hx := C.int(window.props.ClientMaxHeight)
		//xm := C.int(window.props.MouseX)
		//ym := C.int(window.props.MouseY)
		b := toCInt(window.props.Borderless)
		d := toCInt(window.props.Dragable)
		r := toCInt(window.props.Resizable)
		f := toCInt(window.props.Fullscreen)
		l := toCInt(window.props.MouseLocked)
		C.g2d_set_window_props(window.dataC, x, y, w, h, wn, hn, wx, hx, b, d, r, f, l)
	}
}

// MakeCurrent makes OpenGL context current to this thread.
func (ctx *tContext) MakeCurrent() error {
	var errC C.int
	var errWin32C C.g2d_ul_t
	var errStrC *C.char
	C.g2d_ctx_make_current(ctx.dc, ctx.rc, &errC, &errWin32C, &errStrC)
	if errC == 0 {
		return nil
	}
	err := toError(int(errC), uint64(errWin32C), errStrC)
	if errStrC != nil {
		C.g2d_free_mem(unsafe.Pointer(errStrC))
	}
	return err
}

// Release makes all current OpenGL contexts (to this thread) not current.
func (ctx *tContext) Release() error {
	var errC C.int
	var errWin32C C.g2d_ul_t
	var errStrC *C.char
	C.g2d_ctx_release(&errC, &errWin32C, &errStrC)
	if errC == 0 {
		return nil
	}
	err := toError(int(errC), uint64(errWin32C), errStrC)
	if errStrC != nil {
		C.g2d_free_mem(unsafe.Pointer(errStrC))
	}
	return err
}

// SwapBuffers swaps the front and back buffers of the window.
func (ctx *tContext) SwapBuffers() error {
	var errC C.int
	var errWin32C C.g2d_ul_t
	var errStrC *C.char
	C.g2d_swap_buffers(ctx.dc, &errC, &errWin32C, &errStrC)
	if errC == 0 {
		return nil
	}
	err := toError(int(errC), uint64(errWin32C), errStrC)
	if errStrC != nil {
		C.g2d_free_mem(unsafe.Pointer(errStrC))
	}
	return err
}

// toError converts an error number to a Go error object.
func toError(err int, errWin32 uint64, errStrC *C.char) error {
	if err != 0 {
		var errStr string
		switch err {
		case 1:
			errStr = "get module instance failed"
		case 2:
			errStr = "get wglChoosePixelFormatARB failed"
		case 3:
			errStr = "get wglCreateContextAttribsARB failed"
		case 4:
			errStr = "register class failed"
		case 5:
			errStr = "create window failed"
		case 6:
			errStr = "get device context failed"
		case 7:
			errStr = "choose pixel format failed"
		case 8:
			errStr = "set pixel format failed"
		case 9:
			errStr = "create render context failed"
		case 10:
			errStr = "make context current failed"
		case 18:
			errStr = "release context failed (18)"
		case 19:
			errStr = "swap buffer failed (19)"
		default:
			errStr = "oglwnd: unknown error"
		}
		if errWin32 > 0 {
			errStr = errStr + " - " + toString(errWin32)
		}
		if errStrC != nil {
			errStr = errStr + "; " + C.GoString(errStrC)
		}
		return errors.New(errStr)
	}
	return nil
}

func ensureParams(params *Parameters) *Parameters {
	if params == nil {
		params = new(Parameters)
		params.Init()
	}
	if params.handler == nil {
		params.handler = new(DefaultHandler)
	}
	return params
}

func newWrapper(window *Window, handler Handler, autoUpdate, threaded bool) Handler {
	if threaded {
		// TODO threaded wrapper
		wrapper := new(tHandlerWrapper)
		wrapper.window = window
		wrapper.handler = handler
		wrapper.autoUpdate = autoUpdate
		return wrapper
	}
	wrapper := new(tHandlerWrapper)
	wrapper.window = window
	wrapper.handler = handler
	wrapper.autoUpdate = autoUpdate
	return wrapper
}

//export goOnUpdate
func goOnUpdate(hndlC C.int) {
	if window, ok := hndlValue(int(hndlC)).(*Window); ok {
		window.updateProps()
		propsBak := window.props
		window.err = window.handler.OnUpdate(&window.props)
		window.props.Destroy = bool(window.err != nil)
		if window.props != propsBak {
			window.applyProps()
		}
	}
}

//export goOnClose
func goOnClose(hndlC C.int) {
	if window, ok := hndlValue(int(hndlC)).(*Window); ok {
		window.updateProps()
		propsBak := window.props
		window.err = window.handler.OnClose(&window.props)
		window.props.Destroy = bool(window.err != nil)
		if window.props != propsBak {
			window.applyProps()
		}
	}
}

//export goOnCustom
func goOnCustom(hndlC C.int) {
	if window, ok := hndlValue(int(hndlC)).(*Window); ok {
		window.mutex.Lock()
		ev := window.queue.First()
		window.mutex.Unlock()
		if ev != nil {
			window.updateProps()
			propsBak := window.props
			switch ev.(type) {
			case *event.Update:
				window.err = window.handler.OnUpdate(&window.props)
			default:
				window.err = window.handler.OnCustom(&window.props, ev)
			}
			window.props.Destroy = bool(window.err != nil)
			if window.props != propsBak {
				window.applyProps()
			}
		}
	}
}
