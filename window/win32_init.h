/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

static void module_init(int *const err, g2d_ul_t *const err_win32) {
	if (instance == NULL) {
		instance = GetModuleHandle(NULL);
		if (!instance) {
			err[0] = 1;
			err_win32[0] = GetLastError();
		}
	}
}

static void wgl_functions_init(int *const err, g2d_ul_t *const err_win32) {
	if (err[0] == 0) {
		if (!wglChoosePixelFormatARB) {
			wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
			if (!wglChoosePixelFormatARB) {
				err[0] = 2;
				err_win32[0] = GetLastError();
			}
		}
		if (!wglCreateContextAttribsARB) {
			wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
			if (!wglCreateContextAttribsARB) {
				err[0] = 3;
				err_win32[0] = GetLastError();
			}
		}
	}
}

static void monitor_init(int *const err, g2d_ul_t *const err_win32) {
	if (err[0] == 0) {
		if (monitor.hndl == NULL) {
			// monitor.hndl = MonitorFromWindow(dummy.hndl, MONITOR_DEFAULTTONEAREST);
			if (monitor.hndl == NULL)
				monitor.hndl = MonitorFromWindow(NULL, MONITOR_DEFAULTTOPRIMARY);
		}
		MONITORINFO mi = { sizeof(mi) };
		GetMonitorInfo(monitor.hndl, &mi);
		monitor.x = mi.rcMonitor.left;
		monitor.y = mi.rcMonitor.top;
		monitor.width = mi.rcMonitor.right - mi.rcMonitor.left;
		monitor.height = mi.rcMonitor.bottom - mi.rcMonitor.top;
	}
}

static void window_alloc(wnd_data_t **const data, const int go_obj, int *const err, g2d_ul_t *const err_win32, char **const err_str) {
	if (err[0] == 0) {
		data[0] = (wnd_data_t*)malloc(sizeof(wnd_data_t));
		ZeroMemory(data[0], sizeof(wnd_data_t));
		data[0]->go_obj = go_obj;
	}
}

static void config_ensure(config_t *const config) {
	if (config->width <= 0)
		config->width = 640;
	if (config->height <= 0)
		config->height = 480;
	if (config->width_min < 0)
		config->width_min = 0;
	if (config->height_min < 0)
		config->height_min = 0;
	if (config->width_max < 0)
		config->width_max = 99999;
	if (config->height_max < 0)
		config->height_max = 99999;
}

static void window_class_init(wnd_data_t *const data, int *const err, g2d_ul_t *const err_win32, char **const err_str) {
	if (err[0] == 0) {
		data->window.cls.cbSize = sizeof(WNDCLASSEX);
		data->window.cls.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		data->window.cls.lpfnWndProc = windowProc;
		data->window.cls.cbClsExtra = 0;
		data->window.cls.cbWndExtra = 0;
		data->window.cls.hInstance = instance;
		data->window.cls.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		data->window.cls.hCursor = LoadCursor(NULL, IDC_ARROW);
		data->window.cls.hbrBackground = NULL;
		data->window.cls.lpszMenuName = NULL;
		data->window.cls.lpszClassName = CLASS_NAME;
		data->window.cls.hIconSm = NULL;
		if (!is_class_registered(data->window.cls.lpszClassName) && RegisterClassEx(&data->window.cls) == INVALID_ATOM) {
			err[0] = 4;
			err_win32[0] = GetLastError();
		}
	}
}

static void window_create(wnd_data_t *const data, int *const err, g2d_ul_t *const err_win32, char **const err_str) {
	if (err[0] == 0) {
		const DWORD style = WS_OVERLAPPEDWINDOW;
		data->window.hndl = CreateWindow(data->window.cls.lpszClassName, TEXT("OpenGL"), style, 10, 10, 640, 480, NULL, NULL, data->window.cls.hInstance, (void*)data);
		if (!data->window.hndl) {
			err[0] = 5;
			err_win32[0] = GetLastError();
			UnregisterClass(data->window.cls.lpszClassName, instance);
		}
	}
}

static void window_context_init(wnd_data_t *const data, int *const err, g2d_ul_t *const err_win32, char **const err_str) {
	if (err[0] == 0) {
		data->window.dc = GetDC(data->window.hndl);
		if (data->window.dc) {
			int pixelFormat;
			BOOL status = FALSE;
			UINT numFormats = 0;
			const int pixelAttribs[] = {
				WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
				WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
				WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
				/* WGL_SWAP_COPY_ARB has update problems in fullscreen */
				/* WGL_SWAP_EXCHANGE_ARB has problems with start menu in fullscreen */
				WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
				WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
				WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
				WGL_COLOR_BITS_ARB, 32,
				WGL_ALPHA_BITS_ARB, 8,
				WGL_DEPTH_BITS_ARB, 24,
				0
			};
			int  contextAttributes[] = {
				WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
				WGL_CONTEXT_MINOR_VERSION_ARB, 0,
				WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
				0
			};
			status = wglChoosePixelFormatARB(data->window.dc, pixelAttribs, NULL, 1, &pixelFormat, &numFormats);
			if (status && numFormats) {
				PIXELFORMATDESCRIPTOR pfd;
				memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
				DescribePixelFormat(data->window.dc, pixelFormat, sizeof(PIXELFORMATDESCRIPTOR), &pfd);
				if (SetPixelFormat(data->window.dc, pixelFormat, &pfd)) {
					data->window.rc = wglCreateContextAttribsARB(data->window.dc, 0, contextAttributes);
					if (!data->window.rc) {
						err[0] = 9;
						err_win32[0] = GetLastError();
						ReleaseDC(data->window.hndl, data->window.dc);
						DestroyWindow(data->window.hndl);
						UnregisterClass(data->window.cls.lpszClassName, instance);
						data->window.dc = NULL;
						data->window.hndl = NULL;
					}
				} else {
					err[0] = 8;
					err_win32[0] = GetLastError();
					ReleaseDC(data->window.hndl, data->window.dc);
					DestroyWindow(data->window.hndl);
					UnregisterClass(data->window.cls.lpszClassName, instance);
					data->window.dc = NULL;
					data->window.hndl = NULL;
				}
			} else {
				err[0] = 7;
				err_win32[0] = GetLastError();
				ReleaseDC(data->window.hndl, data->window.dc);
				DestroyWindow(data->window.hndl);
				UnregisterClass(data->window.cls.lpszClassName, instance);
				data->window.dc = NULL;
				data->window.hndl = NULL;
			}
		} else {
			err[0] = 6;
			DestroyWindow(data->window.hndl);
			UnregisterClass(data->window.cls.lpszClassName, instance);
			data->window.hndl = NULL;
		}
	}
}
