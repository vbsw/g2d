/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package window creates a window with OpenGL 3.0 context.
package window

import "C"
import (
	"gitlab.com/vbsw/g2d/event"
)

const (
	notInitialized = "oglwnd not initialized"
)

var (
	initialized bool
	hndls       []interface{}
)

// Parameters is the window's initialization parameters.
type Parameters struct {
	ClientX, ClientY, ClientWidth, ClientHeight                      int
	ClientMinWidth, ClientMinHeight, ClientMaxWidth, ClientMaxHeight int
	handler                                                          Handler
	Centered, MouseLocked, Borderless, Dragable                      bool
	Resizable, Fullscreen, Threaded, AutoUpdate                      bool
}

// Context provides OpenGL context functions.
type Context interface {
	MakeCurrent() error
	Release() error
	SwapBuffers() error
}

// Properties holds properties of the window. Used for checking and updating the window.
type Properties struct {
	ClientX, ClientY, ClientWidth, ClientHeight                      int
	ClientMinWidth, ClientMinHeight, ClientMaxWidth, ClientMaxHeight int
	MouseX, MouseY                                                   int
	MouseLocked, Borderless, Dragable, Resizable, Fullscreen         bool
	Destroy                                                          bool
}

// Handler is an abstraction of event handling.
type Handler interface {
	OnInit(Context) error
	OnShow(*Properties) error
	OnUpdate(*Properties) error
	OnClose(*Properties) error
	OnCustom(*Properties, interface{}) error
	OnDestroy()
}

// DefaultHandler is the default handling for events.
type DefaultHandler struct {
}

// tHandlerWrapper is event preprocessor before the handler.
type tHandlerWrapper struct {
	window     *Window
	handler    Handler
	autoUpdate bool
}

func (hnWrapper *tHandlerWrapper) OnInit(ctx Context) error {
	return hnWrapper.handler.OnInit(ctx)
}

func (hnWrapper *tHandlerWrapper) OnShow(props *Properties) error {
	return hnWrapper.handler.OnShow(props)
}

func (hnWrapper *tHandlerWrapper) OnUpdate(props *Properties) error {
	err := hnWrapper.handler.OnUpdate(props)
	if err == nil && hnWrapper.autoUpdate {
		hnWrapper.window.PostEvent(event.NewUpdate())
	}
	return err
}

func (hnWrapper *tHandlerWrapper) OnClose(props *Properties) error {
	return hnWrapper.handler.OnClose(props)
}

func (hnWrapper *tHandlerWrapper) OnCustom(props *Properties, ev interface{}) error {
	return hnWrapper.handler.OnCustom(props, ev)
}

func (hnWrapper *tHandlerWrapper) OnDestroy() {
	hnWrapper.handler.OnDestroy()
}

// Init sets the initialization parameters to default values.
func (params *Parameters) Init() {
	params.ClientWidth = 640
	params.ClientHeight = 480
	params.ClientMaxWidth = 99999
	params.ClientMaxHeight = 99999
	params.Centered = true
	params.Resizable = true
}

// OnInit is called after window has been initialized, but before it has been set visible.
func (hand *DefaultHandler) OnInit(ctx Context) error {
	return nil
}

// OnShow is called after window has been set visible.
func (hand *DefaultHandler) OnShow(props *Properties) error {
	return nil
}

// OnUpdate is called after all window's events has been processed.
// This function must be enabled at window's creation.
func (hand *DefaultHandler) OnUpdate(props *Properties) error {
	return nil
}

// OnClose is called at window's close request.
func (hand *DefaultHandler) OnClose(props *Properties) error {
	props.Destroy = true
	return nil
}

// OnUpdate is called after all window's events has been processed.
// This function must be enabled at window's creation.
func (hand *DefaultHandler) OnCustom(props *Properties, ev interface{}) error {
	return nil
}

// OnDestroy is called before window is closed and all ressources associated with
// it are released.
func (hand *DefaultHandler) OnDestroy() {
}

// toString converts a positive integer value to string.
func toString(value uint64) string {
	var byteArr [20]byte
	var decimals int
	tenth := value / 10
	byteArr[19] = byte(value - tenth*10 + 48)
	value = tenth
	for decimals = 1; value > 0 && decimals < 20; decimals++ {
		tenth := value / 10
		byteArr[19-decimals] = byte(value - tenth*10 + 48)
		value = tenth
	}
	return string(byteArr[20-decimals:])
}

func registerHndl(obj interface{}) int {
	for i, hndl := range hndls {
		if hndl == nil {
			hndls[i] = obj
			return i
		}
	}
	hndls = append(hndls, obj)
	return len(hndls) - 1
}

func deleteHndl(i int) {
	hndls[i] = nil
}

func hndlValue(i int) interface{} {
	return hndls[i]
}

// toCInt converts bool value to C int value.
func toCInt(b bool) C.int {
	if b {
		return C.int(1)
	}
	return C.int(0)
}
