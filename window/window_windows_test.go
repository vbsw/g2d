/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package window

import (
	"testing"
)

func TestInit(t *testing.T) {
	err := Init()
	if err == nil {
		_, errWin := New()
		if errWin != nil {
			t.Error(errWin.Error())
		}
	} else {
		t.Error(err.Error())
	}
}
