module gitlab.com/vbsw/g2d/window

go 1.13

require (
	gitlab.com/vbsw/g2d/event v0.1.0
	gitlab.com/vbsw/g2d/queue v0.1.0
)
