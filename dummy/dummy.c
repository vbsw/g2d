/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#define CLASS_NAME TEXT("g2d_dummy")
typedef unsigned long oglwnd_ul_t;

static HINSTANCE instance = NULL;
static BOOL initialized = FALSE;

static struct {
	WNDCLASSEX cls;
	HWND hndl;
	HDC dc;
	HGLRC rc;
} dummy = { {sizeof(WNDCLASSEX), 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL}, NULL, NULL, NULL };

static void module_init(int *const err, oglwnd_ul_t *const err_win32) {
	if (instance == NULL) {
		instance = GetModuleHandle(NULL);
		if (!instance) {
			err[0] = 1;
			err_win32[0] = GetLastError();
		}
	}
}

static void dummy_class_init(int *const err, oglwnd_ul_t *const err_win32) {
	if (err[0] == 0) {
		dummy.cls.cbSize = sizeof(WNDCLASSEX);
		dummy.cls.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		dummy.cls.lpfnWndProc = DefWindowProc;
		dummy.cls.cbClsExtra = 0;
		dummy.cls.cbWndExtra = 0;
		dummy.cls.hInstance = instance;
		dummy.cls.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		dummy.cls.hCursor = LoadCursor(NULL, IDC_ARROW);
		dummy.cls.hbrBackground = NULL;
		dummy.cls.lpszMenuName = NULL;
		dummy.cls.lpszClassName = CLASS_NAME;
		dummy.cls.hIconSm = NULL;
		if (RegisterClassEx(&dummy.cls) == INVALID_ATOM) {
			err[0] = 2;
			err_win32[0] = GetLastError();
		}
	}
}

static void dummy_window_create(int *const err, oglwnd_ul_t *const err_win32) {
	if (err[0] == 0) {
		dummy.hndl = CreateWindow(dummy.cls.lpszClassName, TEXT("Dummy"), WS_OVERLAPPEDWINDOW, 0, 0, 1, 1, NULL, NULL, instance, NULL);
		if (!dummy.hndl) {
			err[0] = 3;
			err_win32[0] = GetLastError();
		}
	}
}

static void dummy_context_init(int *const err, oglwnd_ul_t *const err_win32) {
	if (err[0] == 0) {
		dummy.dc = GetDC(dummy.hndl);
		if (dummy.dc) {
			int pixelFormat;
			PIXELFORMATDESCRIPTOR pixelFormatDesc;
			ZeroMemory(&pixelFormatDesc, sizeof(PIXELFORMATDESCRIPTOR));
			pixelFormatDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
			pixelFormatDesc.nVersion = 1;
			pixelFormatDesc.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
			pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
			pixelFormatDesc.cColorBits = 32;
			pixelFormatDesc.cAlphaBits = 8;
			pixelFormatDesc.cDepthBits = 24;
			pixelFormat = ChoosePixelFormat(dummy.dc, &pixelFormatDesc);
			if (pixelFormat) {
				if (SetPixelFormat(dummy.dc, pixelFormat, &pixelFormatDesc)) {
					dummy.rc = wglCreateContext(dummy.dc);
					if (!dummy.rc) {
						err[0] = 7;
						err_win32[0] = GetLastError();
					}
				} else {
					err[0] = 6;
					err_win32[0] = GetLastError();
				}
			} else {
				err[0] = 5;
				err_win32[0] = GetLastError();
			}
		} else {
			err[0] = 4;
		}
	}
}

static void dummy_context_make_current(int *const err, oglwnd_ul_t *const err_win32) {
	if (err[0] == 0) {
		if (!wglMakeCurrent(dummy.dc, dummy.rc)) {
			err[0] = 8;
			err_win32[0] = GetLastError();
		}
	}
}

void g2d_dummy_init(int *const err, oglwnd_ul_t *const err_win32) {
	if (!initialized) {
		module_init(err, err_win32);
		dummy_class_init(err, err_win32);
		dummy_window_create(err, err_win32);
		dummy_context_init(err, err_win32);
		dummy_context_make_current(err, err_win32);
		if (err[0]) {
			initialized = TRUE;
		} else {
			if (dummy.rc) {
				wglDeleteContext(dummy.rc);
				dummy.rc = NULL;
			}
			if (dummy.dc) {
				ReleaseDC(dummy.hndl, dummy.dc);
				dummy.dc = NULL;
			}
			if (dummy.hndl) {
				DestroyWindow(dummy.hndl);
				dummy.hndl = NULL;
			}
			if (dummy.cls.lpszClassName)
				UnregisterClass(dummy.cls.lpszClassName, instance);
		}
	}
}

void g2d_dummy_destroy(int *const err, oglwnd_ul_t *const err_win32) {
	if (initialized) {
		if (dummy.rc) {
			if (!wglMakeCurrent(NULL, NULL)) {
				err[0] = 9;
				err_win32[0] = GetLastError();
			}
			if (!wglDeleteContext(dummy.rc)) {
				err[0] = 10;
				err_win32[0] = GetLastError();
			}
			dummy.rc = NULL;
		}
		if (dummy.dc) {
			ReleaseDC(dummy.hndl, dummy.dc);
			dummy.dc = NULL;
		}
		if (dummy.hndl) {
			if (!DestroyWindow(dummy.hndl) && err[0] == 0) {
				err[0] = 11;
				err_win32[0] = GetLastError();
			}
			dummy.hndl = NULL;
		}
		if (dummy.cls.lpszClassName) {
			if (!UnregisterClass(dummy.cls.lpszClassName, instance) && err[0] == 0) {
				err[0] = 12;
				err_win32[0] = GetLastError();
			}
		}
		initialized = FALSE;
	}
}