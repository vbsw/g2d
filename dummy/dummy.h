#ifndef G2DDUMMY_H
#define G2DDUMMY_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long oglwnd_ul_t;
extern void g2d_dummy_init(int *err, oglwnd_ul_t *err_win32);
extern void g2d_dummy_destroy(int *err, oglwnd_ul_t *err_win32);

#ifdef __cplusplus
}
#endif

#endif /* G2DDUMMY_H */