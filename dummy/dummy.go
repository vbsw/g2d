/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package dummy

// #cgo LDFLAGS: -lgdi32 -lOpenGL32
// #include "dummy.h"
import "C"
import (
	"errors"
)

// Init creates a window with OpenGL 1.0 context and make the context current.
func Init() error {
	var errC C.int
	var errWin32C C.oglwnd_ul_t
	C.g2d_dummy_init(&errC, &errWin32C)
	if errC == 0 {
		return nil
	}
	return toError(int(errC), uint64(errWin32C))
}

// Destroy releases current OpenGL context and its window ressources.
func Destroy() error {
	var errC C.int
	var errWin32C C.oglwnd_ul_t
	C.g2d_dummy_destroy(&errC, &errWin32C)
	if errC == 0 {
		return nil
	}
	return toError(int(errC), uint64(errWin32C))
}

// toError converts error number to a error object.
func toError(err int, errWin32 uint64) error {
	if err != 0 {
		var errStr string
		switch err {
		case 1:
			errStr = "get module instance failed"
		case 2:
			errStr = "register dummy class failed"
		case 3:
			errStr = "create dummy window failed"
		case 4:
			errStr = "get dummy device context failed"
		case 5:
			errStr = "choose dummy pixel format failed"
		case 6:
			errStr = "set dummy pixel format failed"
		case 7:
			errStr = "create dummy render context failed"
		case 8:
			errStr = "make dummy context current failed"
		case 9:
			errStr = "release dummy context failed"
		case 10:
			errStr = "deleting dummy render context failed"
		case 11:
			errStr = "destroying dummy window failed"
		case 12:
			errStr = "unregister dummy class failed"
		default:
			errStr = "g2d/dummy: unknown error"
		}
		if errWin32 > 0 {
			errStr = errStr + " - " + toString(errWin32)
		}
		return errors.New(errStr)
	}
	return nil
}

// toString converts an unsigned integer to string.
func toString(value uint64) string {
	var byteArr [20]byte
	var decimals int
	tenth := value / 10
	byteArr[19] = byte(value - tenth*10 + 48)
	value = tenth
	for decimals = 1; value > 0 && decimals < 20; decimals++ {
		tenth := value / 10
		byteArr[19-decimals] = byte(value - tenth*10 + 48)
		value = tenth
	}
	return string(byteArr[20-decimals:])
}
