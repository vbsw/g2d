module gitlab.com/vbsw/g2d

go 1.17

require (
	gitlab.com/vbsw/g2d/win32/dummy v0.2.0
	gitlab.com/vbsw/g2d/win32/graphics v0.1.0
	gitlab.com/vbsw/g2d/win32/instance v0.1.1
)

require gitlab.com/vbsw/g2d/win32/context v0.1.1 // indirect
