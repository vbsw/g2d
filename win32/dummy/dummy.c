/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "dummy.h"

#define CLASS_NAME TEXT("g2d_dummy")

typedef struct {
	int err_num;
	g2d_ul_t err_win32;
	char *err_str;
} error_t;

typedef struct {
	HDC dc;
	HGLRC rc;
} context_t;

typedef struct {
	WNDCLASSEX cls;
	HWND hndl;
	context_t ctx;
} window_t;

static void *error_new(const int err_num, const DWORD err_win32, char *const err_str) {
	error_t *const err = (error_t*)malloc(sizeof(error_t));
	err->err_num = err_num;
	err->err_win32 = (g2d_ul_t)err_win32;
	err->err_str = err_str;
	return NULL;
}

static void *g2d_dummy_instance(void **const err) {
	if (err[0] == NULL) {
		HINSTANCE const instance = GetModuleHandle(NULL);
		if (!instance)
			err[0] = error_new(1, GetLastError(), NULL);
		return (void*)instance;
	}
	return NULL;
}

static BOOL is_class_registered(window_t *const dummy) {
	WNDCLASSEX wcx;
	if (GetClassInfoEx(dummy[0].cls.hInstance, dummy[0].cls.lpszClassName, &wcx))
		return TRUE;
	return FALSE;
}

static void g2d_dummy_class_register(window_t *const dummy, HINSTANCE const instance, void **const err) {
	if (err[0] == NULL) {
		dummy[0].cls.cbSize = sizeof(WNDCLASSEX);
		dummy[0].cls.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		dummy[0].cls.lpfnWndProc = DefWindowProc;
		dummy[0].cls.cbClsExtra = 0;
		dummy[0].cls.cbWndExtra = 0;
		dummy[0].cls.hInstance = instance;
		dummy[0].cls.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		dummy[0].cls.hCursor = LoadCursor(NULL, IDC_ARROW);
		dummy[0].cls.hbrBackground = NULL;
		dummy[0].cls.lpszMenuName = NULL;
		dummy[0].cls.lpszClassName = CLASS_NAME;
		dummy[0].cls.hIconSm = NULL;
		if (!is_class_registered(dummy) && RegisterClassEx(&dummy[0].cls) == INVALID_ATOM) {
			err[0] = error_new(2, GetLastError(), NULL);
			dummy[0].cls.lpszClassName = NULL;
		}
	}
}

static void g2d_dummy_window_create(window_t *const dummy, void **const err) {
	if (err[0] == NULL) {
		dummy[0].hndl = CreateWindow(dummy[0].cls.lpszClassName, TEXT("Dummy"), WS_OVERLAPPEDWINDOW, 0, 0, 1, 1, NULL, NULL, dummy[0].cls.hInstance, NULL);
		if (!dummy[0].hndl) {
			err[0] = error_new(3, GetLastError(), NULL);
			g2d_dummy_destroy((void*)dummy, err);
		}
	}
}

static void g2d_dummy_context_create(window_t *const dummy, void **const err) {
	if (err[0] == NULL) {
		dummy[0].ctx.dc = GetDC(dummy[0].hndl);
		if (dummy[0].ctx.dc) {
			int pixelFormat;
			PIXELFORMATDESCRIPTOR pixelFormatDesc;
			ZeroMemory(&pixelFormatDesc, sizeof(PIXELFORMATDESCRIPTOR));
			pixelFormatDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
			pixelFormatDesc.nVersion = 1;
			pixelFormatDesc.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
			pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
			pixelFormatDesc.cColorBits = 32;
			pixelFormatDesc.cAlphaBits = 8;
			pixelFormatDesc.cDepthBits = 24;
			pixelFormat = ChoosePixelFormat(dummy[0].ctx.dc, &pixelFormatDesc);
			if (pixelFormat) {
				if (SetPixelFormat(dummy[0].ctx.dc, pixelFormat, &pixelFormatDesc)) {
					dummy[0].ctx.rc = wglCreateContext(dummy[0].ctx.dc);
					if (!dummy[0].ctx.rc) {
						err[0] = error_new(7, GetLastError(), NULL);
						g2d_dummy_destroy((void*)dummy, err);
					}
				} else {
					err[0] = error_new(6, GetLastError(), NULL);
					g2d_dummy_destroy((void*)dummy, err);
				}
			} else {
				err[0] = error_new(5, GetLastError(), NULL);
				g2d_dummy_destroy((void*)dummy, err);
			}
		} else {
			err[0] = error_new(4, ERROR_SUCCESS, NULL);
			g2d_dummy_destroy((void*)dummy, err);
		}
	}
}

void g2d_dummy_new(void **const dummy) {
	dummy[0] = malloc(sizeof(window_t));
	ZeroMemory(dummy[0], sizeof(window_t));
}

void g2d_dummy_init(void *const dummy, void *const instance, void **const err) {
	if (err[0] == NULL && instance) {
		if (dummy) {
			window_t *const dmmy = (window_t*)dummy;
			HINSTANCE inst = (HINSTANCE)instance;
			g2d_dummy_class_register(dmmy, inst, err);
			g2d_dummy_window_create(dmmy, err);
			g2d_dummy_context_create(dmmy, err);
		} else {
			err[0] = error_new(15, ERROR_SUCCESS, NULL);
		}
	}
}

void g2d_dummy_init_test(void *dummy, void **const err) {
	void *const inst = g2d_dummy_instance(err);
	g2d_dummy_init(dummy, inst, err);
}

void g2d_dummy_destroy(void *const dummy, void **const err) {
	window_t *const dmmy = (window_t*)dummy;
	if (dmmy[0].ctx.rc) {
		if (!wglDeleteContext(dmmy[0].ctx.rc) && err[0] == NULL) {
			err[0] = error_new(10, GetLastError(), NULL);
		}
		dmmy[0].ctx.rc = NULL;
	}
	if (dmmy[0].ctx.dc) {
		ReleaseDC(dmmy[0].hndl, dmmy[0].ctx.dc);
		dmmy[0].ctx.dc = NULL;
	}
	if (dmmy[0].hndl) {
		if (!DestroyWindow(dmmy[0].hndl) && err[0] == NULL) {
			err[0] = error_new(11, GetLastError(), NULL);
		}
		dmmy[0].hndl = NULL;
	}
	if (dmmy[0].cls.lpszClassName) {
		if (!UnregisterClass(dmmy[0].cls.lpszClassName, dmmy[0].cls.hInstance) && err[0] == NULL) {
			err[0] = error_new(12, GetLastError(), NULL);
			dmmy[0].cls.lpszClassName = NULL;
		}
	}
}

void g2d_dummy_free(void *const data) {
	free(data);
}

void g2d_dummy_error(void *const err, int *const err_num, g2d_ul_t *const err_win32, char **const err_str) {
	error_t *const error = (error_t*)err;
	err_num[0] = error->err_num;
	err_win32[0] = error->err_win32;
	err_str[0] = error->err_str;
}