/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package dummy provides functions to create a window with OpenGL 1.1 context and make the context current.
package dummy

// #cgo CFLAGS: -DUNICODE
// #cgo LDFLAGS: -lgdi32 -lOpenGL32
// #include "dummy.h"
import "C"
import (
	"errors"
	"gitlab.com/vbsw/g2d/win32/context"
	"strconv"
	"unsafe"
)

// Dummy represents a window with OpenGL 1.1 context, that can
// be used to initialize OpenGL functions.
type Dummy struct {
	DataC unsafe.Pointer
}

// New returns a new instance of Dummy. DataC is a pointer to
// a newly allocated C struct. Call Free to release this memory.
func New() *Dummy {
	dummy := new(Dummy)
	C.g2d_dummy_new(&dummy.DataC)
	return dummy
}

// Init initializes win32 ressources of dummy window.
// Call Destroy to release them.
func (dummy *Dummy) Init(instanceC unsafe.Pointer) error {
	var errC unsafe.Pointer
	C.g2d_dummy_init(dummy.DataC, instanceC, &errC)
	return dummy.toError(errC)
}

// initTest is like Init, but with default HINSTANCE.
func (dummy *Dummy) initTest() error {
	var errC unsafe.Pointer
	C.g2d_dummy_init_test(dummy.DataC, &errC)
	return dummy.toError(errC)
}

// Context returns OpenGL context of this dummy window.
func (dummy *Dummy) Context() *context.Context {
	ctx := context.New(dummy.DataC)
	return ctx
}

// Destroy releases win32 ressources.
func (dummy *Dummy) Destroy() error {
	var errC unsafe.Pointer
	if dummy.DataC != nil {
		C.g2d_dummy_destroy(dummy.DataC, &errC)
	}
	return dummy.toError(errC)
}

// Free releases memory of the C struct that DataC points to.
func (dummy *Dummy) Free() {
	if dummy.DataC != nil {
		C.g2d_dummy_free(dummy.DataC)
		dummy.DataC = nil
	}
}

// toError converts C error to a Go error.
func (dummy *Dummy) toError(errC unsafe.Pointer) error {
	if errC != nil {
		var errStr string
		var errNumC C.int
		var errWin32 C.g2d_ul_t
		var errStrC *C.char
		C.g2d_dummy_error(errC, &errNumC, &errWin32, &errStrC)
		C.g2d_dummy_free(errC)
		switch errNumC {
		case 1:
			errStr = "get module instance failed"
		case 2:
			errStr = "register dummy class failed"
		case 3:
			errStr = "create dummy window failed"
		case 4:
			errStr = "get dummy device context failed"
		case 5:
			errStr = "choose dummy pixel format failed"
		case 6:
			errStr = "set dummy pixel format failed"
		case 7:
			errStr = "create dummy render context failed"
		case 8:
			errStr = "make dummy context current failed"
		case 9:
			errStr = "release dummy context failed"
		case 10:
			errStr = "deleting dummy render context failed"
		case 11:
			errStr = "destroying dummy window failed"
		case 12:
			errStr = "unregister dummy class failed"
		case 13:
			errStr = "swap dummy buffer failed"
		case 14:
			errStr = "dummy context C struct not available"
		case 15:
			errStr = "dummy window C struct not available"
		default:
			errStr = "unknown error " + strconv.FormatUint(uint64(errNumC), 10)
		}
		if errWin32 != 0 {
			errStr = errStr + " (" + strconv.FormatUint(uint64(errWin32), 10) + ")"
		}
		if errStrC != nil {
			errStr = errStr + "; " + C.GoString(errStrC)
			C.g2d_dummy_free(unsafe.Pointer(errStrC))
		}
		return errors.New(errStr)
	}
	return nil
}
