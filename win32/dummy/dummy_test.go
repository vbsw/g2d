/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package dummy

import (
	"testing"
)

func TestNew(t *testing.T) {
	dummyA := New()
	dummyB := New()
	if dummyA == nil {
		t.Error("A - memory allocation failed")
		if dummyB != nil {
			dummyB.Free()
		}
	} else if dummyB == nil {
		t.Error("B - memory allocation failed")
		dummyA.Free()
	} else {
		errA := dummyA.initTest()
		errB := dummyB.initTest()
		if errA != nil {
			t.Error(errA.Error())
		} else if errB != nil {
			t.Error(errB.Error())
		} else {
			ctxA := dummyA.Context()
			ctxB := dummyB.Context()
			errA = ctxA.MakeCurrent()
			errB = ctxB.MakeCurrent()
			if errA != nil {
				t.Error(errA.Error())
			} else if errB != nil {
				t.Error(errB.Error())
			} else {
				errA = ctxA.Release()
				errB = ctxB.Release()
				if errA != nil {
					t.Error(errA.Error())
				}
				if errB != nil {
					t.Error(errB.Error())
				}
			}
		}
		errA = dummyA.Destroy()
		errB = dummyB.Destroy()
		if errA != nil {
			t.Error(errA.Error())
		}
		if errB != nil {
			t.Error(errA.Error())
		}
		dummyA.Free()
		dummyB.Free()
	}
}
