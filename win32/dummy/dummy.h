#ifndef G2DDUMMY_H
#define G2DDUMMY_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long g2d_ul_t;
extern void g2d_dummy_new(void **dummy);
extern void g2d_dummy_init(void *dummy, void *instance, void **err);
extern void g2d_dummy_init_test(void *dummy, void **err);
extern void g2d_dummy_destroy(void *dummy, void **err);
extern void g2d_dummy_free(void *const data);
extern void g2d_dummy_error(void *err, int *err_num, g2d_ul_t *err_win32, char **err_str);

#ifdef __cplusplus
}
#endif

#endif /* G2DDUMMY_H */