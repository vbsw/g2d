/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package context

import (
	"testing"
)

func TestAll(t *testing.T) {
	ctx := New(nil)
	if ctx == nil {
		t.Error("memory allocation failed")
	} else {
		err := ctx.MakeCurrent()
		if err == nil {
			t.Error("no error (make current)")
		}
		err = ctx.Release()
		if err == nil {
			t.Error("no error (release)")
		}
		err = ctx.SwapBuffers()
		if err == nil {
			t.Error("no error (swap buffers)")
		}
	}
}
