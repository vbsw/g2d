#ifndef G2DCONTEXT_H
#define G2DCONTEXT_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long g2d_ul_t;
extern void g2d_context_make_current(void *wnd, void **err);
extern void g2d_context_release(void *wnd, void **err);
extern void g2d_context_swap_buffers(void *wnd, void **err);
extern void g2d_context_free(void *const data);
extern void g2d_context_error(void *err, int *err_num, g2d_ul_t *err_win32, char **err_str);

#ifdef __cplusplus
}
#endif

#endif /* G2DCONTEXT_H */