/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package context provides functions to wrap a win32 OpenGL context.
package context

// #cgo CFLAGS: -DUNICODE
// #cgo LDFLAGS: -lgdi32 -lOpenGL32
// #include "context.h"
import "C"
import (
	"errors"
	"strconv"
	"unsafe"
)

// Context represents OpenGL context.
type Context struct {
	Ptr unsafe.Pointer
}

// New allocates a new instance of Context and returns it.
// ctxPtr must point to a C struct of specific window data.
func New(ctxPtr unsafe.Pointer) *Context {
	context := new(Context)
	context.Ptr = ctxPtr
	return context
}

// MakeCurrent makes OpenGL context current.
func (context *Context) MakeCurrent() error {
	var errC unsafe.Pointer
	C.g2d_context_make_current(context.Ptr, &errC)
	return context.toError(errC)
}

// Release makes OpenGL context no longer current.
// If any other context is current, it stays current.
func (context *Context) Release() error {
	var errC unsafe.Pointer
	C.g2d_context_release(context.Ptr, &errC)
	return context.toError(errC)
}

// SwapBuffers swaps the front and back buffers.
func (context *Context) SwapBuffers() error {
	var errC unsafe.Pointer
	C.g2d_context_swap_buffers(context.Ptr, &errC)
	return context.toError(errC)
}

// toError converts C error to a Go error.
func (context *Context) toError(errC unsafe.Pointer) error {
	if errC != nil {
		var errStr string
		var errNumC C.int
		var errWin32 C.g2d_ul_t
		var errStrC *C.char
		C.g2d_context_error(errC, &errNumC, &errWin32, &errStrC)
		C.g2d_context_free(errC)
		switch errNumC {
		case 8:
			errStr = "make dummy context current failed"
		case 9:
			errStr = "release dummy context failed"
		case 13:
			errStr = "swap dummy buffers failed"
		case 14:
			errStr = "dummy context pointer is null"
		case 56:
			errStr = "make context current failed"
		case 57:
			errStr = "release context failed"
		case 61:
			errStr = "swap buffers failed"
		case 63:
			errStr = "context pointer is null"
		default:
			errStr = "unknown error " + strconv.FormatUint(uint64(errNumC), 10)
		}
		if errWin32 != 0 {
			errStr = errStr + " (" + strconv.FormatUint(uint64(errWin32), 10) + ")"
		}
		if errStrC != nil {
			errStr = errStr + "; " + C.GoString(errStrC)
			C.g2d_context_free(unsafe.Pointer(errStrC))
		}
		return errors.New(errStr)
	}
	return nil
}
