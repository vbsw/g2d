/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "context.h"

#define DUMMY_CLASS_NAME TEXT("g2d_dummy")

typedef struct {
	int err_num;
	g2d_ul_t err_win32;
	char *err_str;
} error_t;

typedef struct {
	HDC dc;
	HGLRC rc;
} context_t;

typedef struct {
	WNDCLASSEX cls;
	HWND hndl;
	context_t ctx;
} window_t;

static void *error_new(const int err_num, const DWORD err_win32, char *const err_str) {
	error_t *const err = (error_t*)malloc(sizeof(error_t));
	err->err_num = err_num;
	err->err_win32 = (g2d_ul_t)err_win32;
	err->err_str = err_str;
	return err;
}

static BOOL is_dummy(window_t *const window) {
	#ifdef UNICODE
	return (wcscmp(window[0].cls.lpszClassName, DUMMY_CLASS_NAME) == 0);
	#else
	return (strcmp(window[0].cls.lpszClassName, DUMMY_CLASS_NAME) == 0);
	#endif
}

void g2d_context_make_current(void *const wnd, void **const err) {
	if (err[0] == NULL) {
		if (wnd) {
			window_t *const window = (window_t*)wnd;
			if (!wglMakeCurrent(window[0].ctx.dc, window[0].ctx.rc))
				if (is_dummy(window))
					err[0] = error_new(8, GetLastError(), NULL);
				else
					err[0] = error_new(56, GetLastError(), NULL);
		} else {
			err[0] = error_new(63, ERROR_SUCCESS, NULL);
		}
	}
}

void g2d_context_release(void *const wnd, void **const err) {
	if (err[0] == NULL) {
		if (wnd) {
			window_t *const window = (window_t*)wnd;
			if (window[0].ctx.rc && window[0].ctx.rc == wglGetCurrentContext()) {
				if (!wglMakeCurrent(NULL, NULL)) {
					if (is_dummy(window))
						err[0] = error_new(9, GetLastError(), NULL);
					else
						err[0] = error_new(57, GetLastError(), NULL);
				}
			}
		} else {
			err[0] = error_new(63, ERROR_SUCCESS, NULL);
		}
	}
}

void g2d_context_swap_buffers(void *const wnd, void **const err) {
	if (err[0] == NULL) {
		if (wnd) {
			window_t *const window = (window_t*)wnd;
			if (!SwapBuffers(window[0].ctx.dc))
				if (is_dummy(window))
					err[0] = error_new(13, GetLastError(), NULL);
				else
					err[0] = error_new(61, GetLastError(), NULL);
		} else {
			err[0] = error_new(63, ERROR_SUCCESS, NULL);
		}
	}
}

void g2d_context_free(void *const data) {
	free(data);
}

void g2d_context_error(void *const err, int *const err_num, g2d_ul_t *const err_win32, char **const err_str) {
	error_t *const error = (error_t*)err;
	err_num[0] = error->err_num;
	err_win32[0] = error->err_win32;
	err_str[0] = error->err_str;
}