/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package graphics provides functions to draw 2D graphics based on OpenGL 3.0.
package graphics

// #cgo CFLAGS: -DUNICODE
// #cgo LDFLAGS: -lgdi32 -lOpenGL32
// #include "graphics.h"
import "C"
import (
	"errors"
	"strconv"
	"unsafe"
)

// Init initializes OpenGL functions.
func Init() error {
	var errC unsafe.Pointer
	C.g2d_graphics_init(&errC)
	return toError(errC)
}

// WGLFunctions returns pointer to wglChoosePixelFormatARB
// and wglCreateContextAttribsARB. Needs to be initialized first.
func WGLFunctions() (unsafe.Pointer, unsafe.Pointer) {
	var cpf, cca unsafe.Pointer
	C.g2d_graphics_wgl_ctx_functions(&cpf, &cca)
	return cpf, cca
}

func toError(errC unsafe.Pointer) error {
	if errC != nil {
		var errStr string
		var errNumC C.int
		var errWin32 C.g2d_ul_t
		var errStrC *C.char
		C.g2d_graphics_error(errC, &errNumC, &errWin32, &errStrC)
		C.g2d_graphics_free(errC)
		errNum := int(errNumC)
		if errNum == 100 {
			if errStrC == nil {
				errStr = "get <function> failed"
			} else {
				errStr = "get " + C.GoString(errStrC) + " failed"
				C.g2d_graphics_free(unsafe.Pointer(errStrC))
			}
			if errWin32 != 0 {
				errStr = errStr + " (" + strconv.FormatUint(uint64(errWin32), 10) + ")"
			}
		} else {
			switch errNum {
			case 1:
				errStr = "get module instance failed"
			default:
				errStr = "unknown error " + strconv.FormatUint(uint64(errWin32), 10)
			}
			if errWin32 != 0 {
				errStr = errStr + " (" + strconv.FormatUint(uint64(errWin32), 10) + ")"
			}
			if errStrC != nil {
				errStr = errStr + "; " + C.GoString(errStrC)
				C.g2d_graphics_free(unsafe.Pointer(errStrC))
			}
		}
		return errors.New(errStr)
	}
	return nil
}
