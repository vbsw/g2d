/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gl/GL.h>
#include "graphics.h"

/* from wglext.h */
typedef BOOL(WINAPI * PFNWGLCHOOSEPIXELFORMATARBPROC) (HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats);
typedef HGLRC(WINAPI * PFNWGLCREATECONTEXTATTRIBSARBPROC) (HDC hDC, HGLRC hShareContext, const int *attribList);
typedef BOOL(WINAPI * PFNWGLSWAPINTERVALEXTPROC) (int interval);
typedef int (WINAPI * PFNWGLGETSWAPINTERVALEXTPROC) (void);

// from glcorearb.h
typedef char GLchar;
typedef ptrdiff_t GLsizeiptr;
typedef GLuint(APIENTRY *PFNGLCREATESHADERPROC) (GLenum type);
typedef void (APIENTRY *PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
typedef void (APIENTRY *PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef void (APIENTRY *PFNGLGETSHADERIVPROC) (GLuint shader, GLenum pname, GLint *params);
typedef void (APIENTRY *PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef GLuint(APIENTRY *PFNGLCREATEPROGRAMPROC) (void);
typedef void (APIENTRY *PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (APIENTRY *PFNGLLINKPROGRAMPROC) (GLuint program);
typedef void (APIENTRY *PFNGLVALIDATEPROGRAMPROC) (GLuint program);
typedef void (APIENTRY *PFNGLGETPROGRAMIVPROC) (GLuint program, GLenum pname, GLint *params);
typedef void (APIENTRY *PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (APIENTRY *PFNGLGENBUFFERSPROC) (GLsizei n, GLuint *buffers);
typedef void (APIENTRY *PFNGLGENVERTEXARRAYSPROC) (GLsizei n, GLuint *arrays);
typedef GLint(APIENTRY *PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const GLchar *name);
typedef void (APIENTRY *PFNGLBINDVERTEXARRAYPROC) (GLuint array);
typedef void (APIENTRY *PFNGLENABLEVERTEXATTRIBARRAYPROC) (GLuint index);
typedef void (APIENTRY *PFNGLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid *pointer);
typedef void (APIENTRY *PFNGLBINDBUFFERPROC) (GLenum target, GLuint buffer);
typedef void (APIENTRY *PFNGLBUFFERDATAPROC) (GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
typedef void (APIENTRY *PFNGLGETVERTEXATTRIBPOINTERVPROC) (GLuint index, GLenum pname, GLvoid **pointer);
typedef void (APIENTRY *PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (APIENTRY *PFNGLDELETEVERTEXARRAYSPROC) (GLsizei n, const GLuint *arrays);
typedef void (APIENTRY *PFNGLDELETEBUFFERSPROC) (GLsizei n, const GLuint *buffers);
typedef void (APIENTRY *PFNGLDELETEPROGRAMPROC) (GLuint program);
typedef void (APIENTRY *PFNGLDELETESHADERPROC) (GLuint shader);
typedef GLint(APIENTRY *PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const GLchar *name);
typedef void (APIENTRY *PFNGLUNIFORMMATRIX3FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (APIENTRY *PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (APIENTRY *PFNGLUNIFORMMATRIX2X3FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (APIENTRY *PFNGLGENERATEMIPMAPPROC) (GLenum target);
typedef void (APIENTRY *PFNGLACTIVETEXTUREPROC) (GLenum texture);

typedef struct {
	int err_num;
	g2d_ul_t err_win32;
	char *err_str;
} error_t;

static PFNWGLCHOOSEPIXELFORMATARBPROC    wglChoosePixelFormatARB    = NULL;
static PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;
static PFNWGLSWAPINTERVALEXTPROC         wglSwapIntervalEXT         = NULL;
static PFNWGLGETSWAPINTERVALEXTPROC      wglGetSwapIntervalEXT      = NULL;

static PFNGLCREATESHADERPROC             glCreateShader             = NULL;
static PFNGLSHADERSOURCEPROC             glShaderSource             = NULL;
static PFNGLCOMPILESHADERPROC            glCompileShader            = NULL;
static PFNGLGETSHADERIVPROC              glGetShaderiv              = NULL;
static PFNGLGETSHADERINFOLOGPROC         glGetShaderInfoLog         = NULL;
static PFNGLCREATEPROGRAMPROC            glCreateProgram            = NULL;
static PFNGLATTACHSHADERPROC             glAttachShader             = NULL;
static PFNGLLINKPROGRAMPROC              glLinkProgram              = NULL;
static PFNGLVALIDATEPROGRAMPROC          glValidateProgram          = NULL;
static PFNGLGETPROGRAMIVPROC             glGetProgramiv             = NULL;
static PFNGLGETPROGRAMINFOLOGPROC        glGetProgramInfoLog        = NULL;
static PFNGLGENBUFFERSPROC               glGenBuffers               = NULL;
static PFNGLGENVERTEXARRAYSPROC          glGenVertexArrays          = NULL;
static PFNGLGETATTRIBLOCATIONPROC        glGetAttribLocation        = NULL;
static PFNGLBINDVERTEXARRAYPROC          glBindVertexArray          = NULL;
static PFNGLENABLEVERTEXATTRIBARRAYPROC  glEnableVertexAttribArray  = NULL;
static PFNGLVERTEXATTRIBPOINTERPROC      glVertexAttribPointer      = NULL;
static PFNGLBINDBUFFERPROC               glBindBuffer               = NULL;
static PFNGLBUFFERDATAPROC               glBufferData               = NULL;
static PFNGLGETVERTEXATTRIBPOINTERVPROC  glGetVertexAttribPointerv  = NULL;
static PFNGLUSEPROGRAMPROC               glUseProgram               = NULL;
static PFNGLDELETEVERTEXARRAYSPROC       glDeleteVertexArrays       = NULL;
static PFNGLDELETEBUFFERSPROC            glDeleteBuffers            = NULL;
static PFNGLDELETEPROGRAMPROC            glDeleteProgram            = NULL;
static PFNGLDELETESHADERPROC             glDeleteShader             = NULL;
static PFNGLGETUNIFORMLOCATIONPROC       glGetUniformLocation       = NULL;
static PFNGLUNIFORMMATRIX3FVPROC         glUniformMatrix3fv         = NULL;
static PFNGLUNIFORMMATRIX4FVPROC         glUniformMatrix4fv         = NULL;
static PFNGLUNIFORMMATRIX2X3FVPROC       glUniformMatrix2x3fv       = NULL;
static PFNGLGENERATEMIPMAPPROC           glGenerateMipmap           = NULL;
static PFNGLACTIVETEXTUREPROC            glActiveTexture            = NULL;

static void *error_new(const int err_num, const DWORD err_win32, char *const err_str) {
	error_t *const err = (error_t*)malloc(sizeof(error_t));
	err->err_num = err_num;
	err->err_win32 = (g2d_ul_t)err_win32;
	err->err_str = err_str;
	return err;
}

static char *str_copy(const char *const str) {
	const size_t str_len0 = strlen(str) + 1;
	char *const str_new = (char *)malloc(str_len0);
	memcpy(str_new, str, str_len0);
	return str_new;
}

static PROC wgl_get_proc(const char *const proc_name, void **const err, const int err_num) {
	if (err[0] == NULL) {
		// wglGetProcAddress could return -1, 1, 2 or 3 on failure (https://www.khronos.org/opengl/wiki/Load_OpenGL_Functions).
		PROC const proc = wglGetProcAddress(proc_name);
		const DWORD err_win32 = GetLastError();
		if (err_win32) {
			err[0] = error_new(err_num, err_win32, str_copy(proc_name));
			return NULL;
		}
		return proc;
	}
	return NULL;
}

static void wgl_functions_init(void **const err) {
	wglChoosePixelFormatARB    = (PFNWGLCHOOSEPIXELFORMATARBPROC)    wgl_get_proc("wglChoosePixelFormatARB",    err, 100);
	wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC) wgl_get_proc("wglCreateContextAttribsARB", err, 100);
	wglSwapIntervalEXT         = (PFNWGLSWAPINTERVALEXTPROC)         wgl_get_proc("wglSwapIntervalEXT",         err, 100);
	wglGetSwapIntervalEXT      = (PFNWGLGETSWAPINTERVALEXTPROC)      wgl_get_proc("wglGetSwapIntervalEXT",      err, 100);
}

static void ogl_functions_init(void **const err) {
	glCreateShader            = (PFNGLCREATESHADERPROC)            wgl_get_proc("glCreateShader",            err, 100);
	glShaderSource            = (PFNGLSHADERSOURCEPROC)            wgl_get_proc("glShaderSource",            err, 100);
	glCompileShader           = (PFNGLCOMPILESHADERPROC)           wgl_get_proc("glCompileShader",           err, 100);
	glGetShaderiv             = (PFNGLGETSHADERIVPROC)             wgl_get_proc("glGetShaderiv",             err, 100);
	glGetShaderInfoLog        = (PFNGLGETSHADERINFOLOGPROC)        wgl_get_proc("glGetShaderInfoLog",        err, 100);
	glCreateProgram           = (PFNGLCREATEPROGRAMPROC)           wgl_get_proc("glCreateProgram",           err, 100);
	glAttachShader            = (PFNGLATTACHSHADERPROC)            wgl_get_proc("glAttachShader",            err, 100);
	glLinkProgram             = (PFNGLLINKPROGRAMPROC)             wgl_get_proc("glLinkProgram",             err, 100);
	glValidateProgram         = (PFNGLVALIDATEPROGRAMPROC)         wgl_get_proc("glValidateProgram",         err, 100);
	glGetProgramiv            = (PFNGLGETPROGRAMIVPROC)            wgl_get_proc("glGetProgramiv",            err, 100);
	glGetProgramInfoLog       = (PFNGLGETPROGRAMINFOLOGPROC)       wgl_get_proc("glGetProgramInfoLog",       err, 100);
	glGenBuffers              = (PFNGLGENBUFFERSPROC)              wgl_get_proc("glGenBuffers",              err, 100);
	glGenVertexArrays         = (PFNGLGENVERTEXARRAYSPROC)         wgl_get_proc("glGenVertexArrays",         err, 100);
	glGetAttribLocation       = (PFNGLGETATTRIBLOCATIONPROC)       wgl_get_proc("glGetAttribLocation",       err, 100);
	glBindVertexArray         = (PFNGLBINDVERTEXARRAYPROC)         wgl_get_proc("glBindVertexArray",         err, 100);
	glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC) wgl_get_proc("glEnableVertexAttribArray", err, 100);
	glVertexAttribPointer     = (PFNGLVERTEXATTRIBPOINTERPROC)     wgl_get_proc("glVertexAttribPointer",     err, 100);
	glBindBuffer              = (PFNGLBINDBUFFERPROC)              wgl_get_proc("glBindBuffer",              err, 100);
	glBufferData              = (PFNGLBUFFERDATAPROC)              wgl_get_proc("glBufferData",              err, 100);
	glGetVertexAttribPointerv = (PFNGLGETVERTEXATTRIBPOINTERVPROC) wgl_get_proc("glGetVertexAttribPointerv", err, 100);
	glUseProgram              = (PFNGLUSEPROGRAMPROC)              wgl_get_proc("glUseProgram",              err, 100);
	glDeleteVertexArrays      = (PFNGLDELETEVERTEXARRAYSPROC)      wgl_get_proc("glDeleteVertexArrays",      err, 100);
	glDeleteBuffers           = (PFNGLDELETEBUFFERSPROC)           wgl_get_proc("glDeleteBuffers",           err, 100);
	glDeleteProgram           = (PFNGLDELETEPROGRAMPROC)           wgl_get_proc("glDeleteProgram",           err, 100);
	glDeleteShader            = (PFNGLDELETESHADERPROC)            wgl_get_proc("glDeleteShader",            err, 100);
	glGetUniformLocation      = (PFNGLGETUNIFORMLOCATIONPROC)      wgl_get_proc("glGetUniformLocation",      err, 100);
	glUniformMatrix3fv        = (PFNGLUNIFORMMATRIX3FVPROC)        wgl_get_proc("glUniformMatrix3fv",        err, 100);
	glUniformMatrix4fv        = (PFNGLUNIFORMMATRIX4FVPROC)        wgl_get_proc("glUniformMatrix4fv",        err, 100);
	glUniformMatrix2x3fv      = (PFNGLUNIFORMMATRIX2X3FVPROC)      wgl_get_proc("glUniformMatrix2x3fv",      err, 100);
	glGenerateMipmap          = (PFNGLGENERATEMIPMAPPROC)          wgl_get_proc("glGenerateMipmap",          err, 100);
	glActiveTexture           = (PFNGLACTIVETEXTUREPROC)           wgl_get_proc("glActiveTexture",           err, 100);
}

void g2d_graphics_init(void **const err) {
	wgl_functions_init(err);
	ogl_functions_init(err);
}

void g2d_graphics_wgl_ctx_functions(void **const cpf, void **const cca) {
	cpf[0] = (void*)wglChoosePixelFormatARB;
	cca[0] = (void*)wglCreateContextAttribsARB;
}

void g2d_graphics_free(void *const data) {
	free(data);
}

void g2d_graphics_error(void *const err, int *const err_num, g2d_ul_t *const err_win32, char **const err_str) {
	error_t *const error = (error_t*)err;
	err_num[0] = error->err_num;
	err_win32[0] = error->err_win32;
	err_str[0] = error->err_str;
}