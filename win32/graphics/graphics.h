#ifndef G2DGRAPHICS_H
#define G2DGRAPHICS_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long g2d_ul_t;
extern void g2d_graphics_init(void **const err);
extern void g2d_graphics_wgl_ctx_functions(void **cpf, void **cca);
extern void g2d_graphics_free(void *const data);
extern void g2d_graphics_error(void *err, int *err_num, g2d_ul_t *err_win32, char **err_str);

#ifdef __cplusplus
}
#endif

#endif /* G2DGRAPHICS_H */