/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// win32 is a package for Go to create a window with OpenGL 3.0 context.
package win32

// #cgo LDFLAGS: -lgdi32 -lOpenGL32
// #include "win32.h"
import "C"
import (
	"unsafe"
	"errors"
)

// New allocates data in C and returns a pointer to it.
func NewWindow() unsafe.Pointer {
	return nil
}