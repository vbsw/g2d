# instance

[![GoDoc](https://godoc.org/gitlab.com/vbsw/g2d/win32/instance?status.svg)](https://godoc.org/gitlab.com/vbsw/g2d/win32/instance) [![Stability: Experimental](https://masterminds.github.io/stability/experimental.svg)](https://masterminds.github.io/stability/experimental.html)

## About
instance is a package for Go to get module handle for win32. It is published on <https://gitlab.com/vbsw/g2d/win32/instance>.

## Copyright
Copyright 2022, Vitali Baumtrok (vbsw@mailbox.org).

instance is distributed under the Boost Software License, version 1.0. (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)

instance is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Boost Software License for more details.

## Compile
Install Go (https://golang.org/doc/install). For Cgo install a C compiler (<https://jmeubank.github.io/tdm-gcc/>).

## References
- https://golang.org/doc/install
- https://jmeubank.github.io/tdm-gcc/
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- https://github.com/golang/go/wiki/cgo
