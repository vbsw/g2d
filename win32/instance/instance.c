/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

typedef unsigned long g2d_ul_t;

typedef struct {
	int err_num;
	g2d_ul_t err_win32;
	char *err_str;
} error_t;

static void *hInstance = NULL;

static void *error_new(const int err_num, const DWORD err_win32, char *const err_str) {
	error_t *const err = (error_t*)malloc(sizeof(error_t));
	err->err_num = err_num;
	err->err_win32 = (g2d_ul_t)err_win32;
	err->err_str = err_str;
	return (void*)err;
}

void g2d_instance_init(void **const err) {
	if (err[0] == NULL) {
		hInstance = (void*)GetModuleHandle(NULL);
		if (!hInstance)
			err[0] = error_new(1, GetLastError(), NULL);
	}
}

void g2d_instance(void **instance) {
	instance[0] = hInstance;
}

void g2d_instance_free(void *const data) {
	free(data);
}

void g2d_instance_error(void *const err, int *const err_num, g2d_ul_t *const err_win32, char **const err_str) {
	error_t *const error = (error_t*)err;
	err_num[0] = error->err_num;
	err_win32[0] = error->err_win32;
	err_str[0] = error->err_str;
}