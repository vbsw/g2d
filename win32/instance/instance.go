/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package instance provides module handle for this executable.
package instance

// #cgo CFLAGS: -DUNICODE
// #cgo LDFLAGS: -lgdi32
// #include "instance.h"
import "C"
import (
	"errors"
	"strconv"
	"unsafe"
)

// Ptr is HINSTANCE. HINSTANCE is the module handle for this executable.
var Ptr unsafe.Pointer

// Init initializes Ptr.
func Init() error {
	var errC unsafe.Pointer
	C.g2d_instance_init(&errC)
	if errC == nil {
		C.g2d_instance(&Ptr)
		return nil
	}
	return toError(errC)
}

func toError(errC unsafe.Pointer) error {
	var errStr string
	var errNumC C.int
	var errWin32 C.g2d_ul_t
	var errStrC *C.char
	C.g2d_instance_error(errC, &errNumC, &errWin32, &errStrC)
	C.g2d_instance_free(errC)
	if errNumC == 1 {
		errStr = "get module handle failed"
	} else {
		errStr = "unknown error " + strconv.FormatUint(uint64(errNumC), 10)
	}
	if errWin32 != 0 {
		errStr = errStr + " (" + strconv.FormatUint(uint64(errWin32), 10) + ")"
	}
	if errStrC != nil {
		errStr = errStr + "; " + C.GoString(errStrC)
		C.g2d_instance_free(unsafe.Pointer(errStrC))
	}
	return errors.New(errStr)
}
