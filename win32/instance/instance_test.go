/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package instance

import (
	"testing"
)

func TestInstance(t *testing.T) {
	err := Init()
	if err != nil {
		t.Error(err.Error())
	} else if Ptr == nil {
		t.Error("instance is nil")
	}
}
