#ifndef G2DINSTANCE_H
#define G2DINSTANCE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long g2d_ul_t;
extern void g2d_instance_init(void **err);
extern void g2d_instance(void **instance);
extern void g2d_instance_free(void *const data);
extern void g2d_instance_error(void *err, int *err_num, g2d_ul_t *err_win32, char **err_str);

#ifdef __cplusplus
}
#endif

#endif /* G2DINSTANCE_H */