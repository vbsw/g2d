/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package window

import (
	"testing"
)

func TestNew(t *testing.T) {
	wnd := New()
	if wnd == nil || wnd.DataC == nil {
		t.Error("memory allocation failed")
	} else {
		err := wnd.SetTitle("hi")
		if err != nil {
			t.Error(err.Error())
		} else {
			err := wnd.initTest()
			if err != nil {
				t.Error(err.Error())
			} else {
				err = wnd.SetTitle("yo")
				if err != nil {
					t.Error(err.Error())
				}
				err = wnd.Destroy()
				if err != nil {
					t.Error(err.Error())
				}
			}
		}
		wnd.Free()
	}
}

func TestContext(t *testing.T) {
	wnd := New()
	if wnd == nil || wnd.DataC == nil {
		t.Error("memory allocation failed")
	} else {
		err := wnd.initTest()
		if err != nil {
			t.Error(err.Error())
		} else {
			ctx := wnd.Context()
			err = ctx.MakeCurrent()
			if err != nil {
				t.Error(err.Error())
			} else {
				err = ctx.Release()
				if err != nil {
					t.Error(err.Error())
				}
			}
			err = wnd.Destroy()
			if err != nil {
				t.Error(err.Error())
			}
		}
		wnd.Free()
	}
}
