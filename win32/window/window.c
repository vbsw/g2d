/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gl/GL.h>
#include "window.h"

// from wgl.h
#define WGL_DRAW_TO_WINDOW_ARB            0x2001
#define WGL_SWAP_METHOD_ARB               0x2007
#define WGL_SUPPORT_OPENGL_ARB            0x2010
#define WGL_DOUBLE_BUFFER_ARB             0x2011
#define WGL_PIXEL_TYPE_ARB                0x2013
#define WGL_TYPE_RGBA_ARB                 0x202B
#define WGL_ACCELERATION_ARB              0x2003
#define WGL_FULL_ACCELERATION_ARB         0x2027
#define WGL_SWAP_EXCHANGE_ARB             0x2028
#define WGL_SWAP_COPY_ARB                 0x2029
#define WGL_SWAP_UNDEFINED_ARB            0x202A
#define WGL_COLOR_BITS_ARB                0x2014
#define WGL_ALPHA_BITS_ARB                0x201B
#define WGL_DEPTH_BITS_ARB                0x2022
#define WGL_CONTEXT_MAJOR_VERSION_ARB     0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB     0x2092
#define WGL_CONTEXT_PROFILE_MASK_ARB      0x9126
#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB  0x00000001

/* from wglext.h */
typedef BOOL(WINAPI * PFNWGLCHOOSEPIXELFORMATARBPROC) (HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, int *piFormats, UINT *nNumFormats);
typedef HGLRC(WINAPI * PFNWGLCREATECONTEXTATTRIBSARBPROC) (HDC hDC, HGLRC hShareContext, const int *attribList);

#define CLASS_NAME TEXT("g2d_window")

typedef struct {
	int err_num;
	g2d_ul_t err_win32;
	char *err_str;
} error_t;

typedef struct {
	HDC dc;
	HGLRC rc;
} context_t;

typedef struct {
	WNDCLASSEX cls;
	HWND hndl;
	context_t ctx;
} window_t;

typedef struct {
	int x, y, width, height;
	int x_wnd, y_wnd, width_wnd, height_wnd;
} client_t;

typedef struct {
	window_t wnd;
	client_t client;
	HMONITOR monitor;
	LPTSTR title;
	DWORD style;
	BOOL initialized;
	void *ext1;
	void *cust;
	void (*class_register)(void*, void*, void**);
	void (*window_create)(void*, void**);
	void (*context_create)(void*, void**);
	BOOL (*message_proc)(void*, HWND, UINT, WPARAM, LPARAM, LRESULT*);
	void (*destroy)(void*, void**);
	void (*free)(void*);
	PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB;
	PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB;
} window_data_t;

static void *error_new(const int err_num, const DWORD err_win32, char *const err_str) {
	error_t *const err = (error_t*)malloc(sizeof(error_t));
	err->err_num = err_num;
	err->err_win32 = (g2d_ul_t)err_win32;
	err->err_str = err_str;
	return err;
}

static LPCTSTR ensure_title(window_data_t *const wnd_data) {
	if (wnd_data[0].title)
		return wnd_data[0].title;
	return TEXT("g2d");
}

static void g2d_screen_metrics(HMONITOR const monitor, int *const x, int *const y, int *const w, int *const h) {
	MONITORINFO mi = { sizeof(mi) };
	GetMonitorInfo(monitor, &mi);
	x[0] = mi.rcMonitor.left;
	y[0] = mi.rcMonitor.top;
	w[0] = mi.rcMonitor.right - mi.rcMonitor.left;
	h[0] = mi.rcMonitor.bottom - mi.rcMonitor.top;
}

static void *g2d_window_instance(void **const err) {
	if (err[0] == NULL) {
		HINSTANCE const instance = GetModuleHandle(NULL);
		if (!instance)
			err[0] = error_new(1, GetLastError(), NULL);
		return (void*)instance;
	}
	return NULL;
}

static BOOL is_class_registered(window_data_t *const wnd_data) {
	WNDCLASSEX wcx;
	if (GetClassInfoEx(wnd_data[0].wnd.cls.hInstance, wnd_data[0].wnd.cls.lpszClassName, &wcx))
		return TRUE;
	return FALSE;
}

static BOOL g2d_message_proc_default(void *const data, HWND const hWnd, const UINT message, const WPARAM wParam, const LPARAM lParam, LRESULT *const result) {
	return 0;
}

static LRESULT CALLBACK window_proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	LRESULT result = 0;
	if (message == WM_NCCREATE) {
		window_data_t *const wnd_data = (window_data_t*)(((CREATESTRUCT*)lParam)->lpCreateParams);
		if (wnd_data)
			SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)wnd_data);
		result = DefWindowProc(hWnd, message, wParam, lParam);
	} else {
		void *const data = (void*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		window_data_t *const wnd_data = (window_data_t*)data;
		if (data == NULL || !wnd_data[0].message_proc(data, hWnd, message, wParam, lParam, &result))
			result = DefWindowProc(hWnd, message, wParam, lParam);
	}
	return result;
}

static void g2d_class_register_default(void *const data, void *const instance, void **const err) {
	if (err[0] == NULL && instance) {
		window_data_t *const wnd_data = (window_data_t*)data;
		HINSTANCE const inst = (HINSTANCE)instance;
		wnd_data[0].wnd.cls.cbSize = sizeof(WNDCLASSEX);
		wnd_data[0].wnd.cls.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wnd_data[0].wnd.cls.lpfnWndProc = window_proc;
		wnd_data[0].wnd.cls.cbClsExtra = 0;
		wnd_data[0].wnd.cls.cbWndExtra = 0;
		wnd_data[0].wnd.cls.hInstance = inst;
		wnd_data[0].wnd.cls.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		wnd_data[0].wnd.cls.hCursor = LoadCursor(NULL, IDC_ARROW);
		wnd_data[0].wnd.cls.hbrBackground = NULL;
		wnd_data[0].wnd.cls.lpszMenuName = NULL;
		wnd_data[0].wnd.cls.lpszClassName = CLASS_NAME;
		wnd_data[0].wnd.cls.hIconSm = NULL;
		if (!is_class_registered(wnd_data) && RegisterClassEx(&wnd_data[0].wnd.cls) == INVALID_ATOM) {
			err[0] = error_new(50, GetLastError(), NULL);
			wnd_data[0].wnd.cls.lpszClassName = NULL;
			wnd_data[0].destroy(data, err);
		}
	}
}

static void g2d_window_create_default(void *const data, void **const err) {
	if (err[0] == NULL) {
		window_data_t *const wnd_data = (window_data_t*)data;
		LPCTSTR const title = ensure_title(wnd_data);
		const int x = wnd_data[0].client.x_wnd;
		const int y = wnd_data[0].client.y_wnd;
		const int width = wnd_data[0].client.width_wnd;
		const int height = wnd_data[0].client.height_wnd;
		const DWORD style = wnd_data[0].style;
		wnd_data[0].wnd.hndl = CreateWindow(wnd_data[0].wnd.cls.lpszClassName, title, style, x, y, width, height, NULL, NULL, wnd_data[0].wnd.cls.hInstance, (LPVOID)data);
		if (wnd_data[0].wnd.hndl) {
			if (wnd_data[0].title) {
				free(wnd_data[0].title);
				wnd_data[0].title = NULL;
			}
		} else {
			err[0] = error_new(51, GetLastError(), NULL);
			wnd_data[0].destroy(data, err);
		}
	}
}

static void g2d_context_create_default(void *const data, void **const err) {
	if (err[0] == NULL) {
		window_data_t *const wnd_data = (window_data_t*)data;
		wnd_data[0].wnd.ctx.dc = GetDC(wnd_data[0].wnd.hndl);
		if (wnd_data[0].wnd.ctx.dc) {
			int pixelFormat;
			BOOL status = FALSE;
			UINT numFormats = 0;
			const int pixelAttribs[] = {
				WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
				WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
				WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
				/* WGL_SWAP_COPY_ARB might have update problems in fullscreen */
				/* WGL_SWAP_EXCHANGE_ARB might have problems with start menu in fullscreen */
				WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
				WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
				WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
				WGL_COLOR_BITS_ARB, 32,
				WGL_ALPHA_BITS_ARB, 8,
				WGL_DEPTH_BITS_ARB, 24,
				0
			};
			const int contextAttributes[] = {
				WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
				WGL_CONTEXT_MINOR_VERSION_ARB, 0,
				WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
				0
			};
			status = wnd_data[0].wglChoosePixelFormatARB(wnd_data[0].wnd.ctx.dc, pixelAttribs, NULL, 1, &pixelFormat, &numFormats);
			if (status && numFormats) {
				PIXELFORMATDESCRIPTOR pfd;
				ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
				DescribePixelFormat(wnd_data[0].wnd.ctx.dc, pixelFormat, sizeof(PIXELFORMATDESCRIPTOR), &pfd);
				if (SetPixelFormat(wnd_data[0].wnd.ctx.dc, pixelFormat, &pfd)) {
					wnd_data[0].wnd.ctx.rc = wnd_data[0].wglCreateContextAttribsARB(wnd_data[0].wnd.ctx.dc, 0, contextAttributes);
					if (!wnd_data[0].wnd.ctx.rc) {
						err[0] = error_new(55, GetLastError(), NULL);
						wnd_data[0].destroy(data, err);
					}
				} else {
					err[0] = error_new(54, GetLastError(), NULL);
					wnd_data[0].destroy(data, err);
				}
			} else {
				err[0] = error_new(53, GetLastError(), NULL);
				wnd_data[0].destroy(data, err);
			}
		} else {
			err[0] = error_new(52, ERROR_SUCCESS, NULL);
			wnd_data[0].destroy(data, err);
		}
	}
}

static void g2d_context_create_default_test(void *const data, void **const err) {
	if (err[0] == NULL) {
		window_data_t *const wnd_data = (window_data_t*)data;
		wnd_data[0].wnd.ctx.dc = GetDC(wnd_data[0].wnd.hndl);
		if (wnd_data[0].wnd.ctx.dc) {
			int pixelFormat;
			PIXELFORMATDESCRIPTOR pixelFormatDesc;
			ZeroMemory(&pixelFormatDesc, sizeof(PIXELFORMATDESCRIPTOR));
			pixelFormatDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
			pixelFormatDesc.nVersion = 1;
			pixelFormatDesc.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
			pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
			pixelFormatDesc.cColorBits = 32;
			pixelFormatDesc.cAlphaBits = 8;
			pixelFormatDesc.cDepthBits = 24;
			pixelFormat = ChoosePixelFormat(wnd_data[0].wnd.ctx.dc, &pixelFormatDesc);
			if (pixelFormat) {
				if (SetPixelFormat(wnd_data[0].wnd.ctx.dc, pixelFormat, &pixelFormatDesc)) {
					wnd_data[0].wnd.ctx.rc = wglCreateContext(wnd_data[0].wnd.ctx.dc);
					if (!wnd_data[0].wnd.ctx.rc) {
						err[0] = error_new(55, GetLastError(), NULL);
						wnd_data[0].destroy(data, err);
					}
				} else {
					err[0] = error_new(54, GetLastError(), NULL);
					wnd_data[0].destroy(data, err);
				}
			} else {
				err[0] = error_new(53, GetLastError(), NULL);
				wnd_data[0].destroy(data, err);
			}
		} else {
			err[0] = error_new(52, ERROR_SUCCESS, NULL);
			wnd_data[0].destroy(data, err);
		}
	}
}

static void g2d_destroy_default(void *const data, void **const err) {
	window_data_t *const wnd_data = (window_data_t*)data;
	BOOL quit = FALSE;
	if (wnd_data[0].wnd.ctx.rc) {
		if (!wglDeleteContext(wnd_data[0].wnd.ctx.rc) && err[0] == NULL) {
			err[0] = error_new(58, GetLastError(), NULL);
		}
		wnd_data[0].wnd.ctx.rc = NULL;
	}
	if (wnd_data[0].wnd.ctx.dc) {
		ReleaseDC(wnd_data[0].wnd.hndl, wnd_data[0].wnd.ctx.dc);
		wnd_data[0].wnd.ctx.dc = NULL;
	}
	if (wnd_data[0].wnd.hndl) {
		if (!DestroyWindow(wnd_data[0].wnd.hndl) && err[0] == NULL) {
			err[0] = error_new(59, GetLastError(), NULL);
		}
		wnd_data[0].wnd.hndl = NULL;
	}
	if (wnd_data[0].wnd.cls.lpszClassName) {
		quit = TRUE;
		if (!UnregisterClass(wnd_data[0].wnd.cls.lpszClassName, wnd_data[0].wnd.cls.hInstance) && err[0] == NULL) {
			err[0] = error_new(60, GetLastError(), NULL);
			wnd_data[0].wnd.cls.lpszClassName = NULL;
		}
	}
	if (wnd_data[0].title) {
		free(wnd_data[0].title);
		wnd_data[0].title = NULL;
	}
	wnd_data[0].initialized = FALSE;
	/* stop event queue thread */
	if (quit && !is_class_registered(wnd_data))
		PostQuitMessage(0);
}

static void g2d_free_default(void *const data) {
	window_data_t *const wnd_data = (window_data_t*)data;
	if (wnd_data[0].title)
		free(wnd_data[0].title);
	free(data);
}

void g2d_window_process_events() {
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0) > 0) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void g2d_window_new(void **const data) {
	data[0] = malloc(sizeof(window_data_t));
	ZeroMemory(data[0], sizeof(window_data_t));
	window_data_t *const wnd_data = (window_data_t*)data[0];
	wnd_data[0].client.x = 50;
	wnd_data[0].client.y = 50;
	wnd_data[0].client.width = 640;
	wnd_data[0].client.height = 480;
	wnd_data[0].client.x_wnd = 50;
	wnd_data[0].client.y_wnd = 50;
	wnd_data[0].client.width_wnd = 640;
	wnd_data[0].client.height_wnd = 480;
	wnd_data[0].monitor = MonitorFromWindow(NULL, MONITOR_DEFAULTTOPRIMARY);
	wnd_data[0].style = WS_OVERLAPPEDWINDOW;
	wnd_data[0].class_register = g2d_class_register_default;
	wnd_data[0].window_create = g2d_window_create_default;
	wnd_data[0].context_create = g2d_context_create_default;
	wnd_data[0].message_proc = g2d_message_proc_default;
	wnd_data[0].destroy = g2d_destroy_default;
	wnd_data[0].free = g2d_free_default;
}

void g2d_window_init(void *const data, void *const instance, void **const err) {
	if (data) {
		window_data_t *const wnd_data = (window_data_t*)data;
		wnd_data[0].class_register(data, instance, err);
		wnd_data[0].window_create(data, err);
		wnd_data[0].context_create(data, err);
		wnd_data[0].initialized = (BOOL)(err[0] == NULL);
	} else {
		err[0] = error_new(64, ERROR_SUCCESS, NULL);
	}
}

void g2d_window_init_test(void *const data, void **const err) {
	void *const instance = g2d_window_instance(err);
	window_data_t *const wnd_data = (window_data_t*)data;
	wnd_data[0].context_create = g2d_context_create_default_test;
	g2d_window_init(data, instance, err);
}

void g2d_window_wnd(void *const data, void **const wnd, void **const err) {
	if (err[0] == NULL) {
		if (data) {
			window_data_t *const wnd_data = (window_data_t*)data;
			wnd[0] = (void*)&wnd_data[0].wnd;
		} else {
			wnd[0] = NULL;
		}
	}
}

void g2d_window_show(void *const data, void **const err) {
	if (data) {
		RECT rect;
		window_data_t *const wnd_data = (window_data_t*)data;
		ShowWindow(wnd_data[0].wnd.hndl, SW_SHOWDEFAULT);
	} else {
		err[0] = error_new(64, ERROR_SUCCESS, NULL);
	}
}

void g2d_window_set_title(void *const data, char *const title, void **const err) {
	if (err[0] == NULL) {
		if (data) {
			window_data_t *const wnd_data = (window_data_t*)data;
			const size_t length = strlen(title) + 1;
			if (wnd_data[0].initialized) {
				#ifdef UNICODE
				LPTSTR const title_tc = (LPTSTR)malloc(sizeof(TCHAR) * length);
				MultiByteToWideChar(CP_UTF8, 0, title, length, title_tc, length);
				if (!SetWindowText(wnd_data[0].wnd.hndl, title_tc))
					err[0] = error_new(62, GetLastError(), NULL);
				free(title_tc);
				#else
				if (!SetWindowText(wnd_data[0].wnd.hndl, title))
					err[0] = error_new(62, GetLastError(), NULL);
				#endif
			} else {
				if (wnd_data[0].title)
					free(wnd_data[0].title);
				wnd_data[0].title = (LPTSTR)malloc(sizeof(TCHAR) * length);
				#ifdef UNICODE
				MultiByteToWideChar(CP_UTF8, 0, title, length, wnd_data[0].title, length);
				#else
				memcpy(wnd_data[0].title, title, length);
				#endif
			}
		} else {
			err[0] = error_new(64, ERROR_SUCCESS, NULL);
		}
	}
}

void g2d_window_set_fullscreen(void *const data, const int fullscreen) {
	window_data_t *const wnd_data = (window_data_t*)data;
	if (fullscreen) {
		int x, y, w, h;
		g2d_screen_metrics(wnd_data[0].monitor, &x, &y, &w, &h);
		// calls WM_SIZE
		SetWindowLong(wnd_data[0].wnd.hndl, GWL_STYLE, wnd_data[0].style & ~WS_OVERLAPPEDWINDOW);
		SetWindowPos(wnd_data[0].wnd.hndl, HWND_TOP, x, y, w, h, SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_SHOWWINDOW);
	} else {
		RECT rect = { wnd_data[0].client.x_wnd, wnd_data[0].client.y_wnd, wnd_data[0].client.x_wnd + wnd_data[0].client.width_wnd, wnd_data[0].client.y_wnd + wnd_data[0].client.height_wnd };
		AdjustWindowRect(&rect, wnd_data[0].style, FALSE);
		const int x = rect.left;
		const int y = rect.top;
		const int w = rect.right - rect.left;
		const int h = rect.bottom - rect.top;
		SetWindowLong(wnd_data[0].wnd.hndl, GWL_STYLE, wnd_data[0].style);
		SetWindowPos(wnd_data[0].wnd.hndl, HWND_NOTOPMOST, x, y, w, h, SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_SHOWWINDOW);
	}
}

void g2d_window_destroy(void *const data, void **const err) {
	window_data_t *const wnd_data = (window_data_t*)data;
	wnd_data[0].destroy(data, err);
}

void g2d_window_data_free(void *const data) {
	window_data_t *const wnd_data = (window_data_t*)data;
	wnd_data[0].free(data);
}

void g2d_window_free(void *const data) {
	free(data);
}

void g2d_window_error(void *const err, int *const err_num, g2d_ul_t *const err_win32, char **const err_str) {
	error_t *const error = (error_t*)err;
	err_num[0] = error->err_num;
	err_win32[0] = error->err_win32;
	err_str[0] = error->err_str;
}