#ifndef G2DWINDOW_H
#define G2DWINDOW_H

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long g2d_ul_t;
extern void g2d_window_process_events();
extern void g2d_window_new(void **data);
extern void g2d_window_init(void *data, void *instance, void **err);
extern void g2d_window_init_test(void *data, void **err);
extern void g2d_window_wnd(void *data, void **wnd, void **err);
extern void g2d_window_show(void *data, void **err);
extern void g2d_window_set_title(void *data, char *title, void **err);
extern void g2d_window_set_fullscreen(void *data, int fullscreen);
extern void g2d_window_destroy(void *data, void **err);
extern void g2d_window_data_free(void *data);
extern void g2d_window_free(void *data);
extern void g2d_window_error(void *err, int *err_num, g2d_ul_t *err_win32, char **err_str);

#ifdef __cplusplus
}
#endif

#endif /* G2DWINDOW_H */