/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package window provides functions to create a window with OpenGL 3.0 context.
package window

// #cgo CFLAGS: -DUNICODE
// #cgo LDFLAGS: -lgdi32 -lOpenGL32
// #include "window.h"
import "C"
import (
	"errors"
	"gitlab.com/vbsw/g2d/win32/context"
	"strconv"
	"unsafe"
)

// Window represents a window with OpenGL 3.0 context.
type Window struct {
	DataC unsafe.Pointer
}

// ProcessEvents retrieves messages from thread's message queue for all windows.
// This functions blocks until thread's message queue is terminated.
func ProcessEvents() {
	C.g2d_window_process_events()
}

// New returns a new instance of Window. DataC is a pointer to
// a newly allocated C struct. Call Free to release this memory.
func New() *Window {
	wnd := new(Window)
	C.g2d_window_new(&wnd.DataC)
	return wnd
}

// Init initializes win32 ressources of window.
// Call Destroy to release them.
func (wnd *Window) Init(instanceC unsafe.Pointer) error {
	var errC unsafe.Pointer
	C.g2d_window_init(wnd.DataC, instanceC, &errC)
	err := wnd.toError(errC)
	return err
}

// initTest is like Init, but with default HINSTANCE and OpenGL 1.1 context.
func (wnd *Window) initTest() error {
	var errC unsafe.Pointer
	C.g2d_window_init_test(wnd.DataC, &errC)
	return wnd.toError(errC)
}

// Context returns OpenGL context of this window.
func (wnd *Window) Context() *context.Context {
	var errUnsuedC unsafe.Pointer
	var wndC unsafe.Pointer
	C.g2d_window_wnd(wnd.DataC, &wndC, &errUnsuedC)
	return context.New(wndC)
}

// Show makes window visible.
func (wnd *Window) Show() error {
	var errC unsafe.Pointer
	C.g2d_window_show(wnd.DataC, &errC)
	return wnd.toError(errC)
}

// SetTitle sets the title of this window.
func (wnd *Window) SetTitle(title string) error {
	var errC unsafe.Pointer
	titleC := C.CString(title)
	C.g2d_window_set_title(wnd.DataC, titleC, &errC)
	C.g2d_window_free(unsafe.Pointer(titleC))
	return wnd.toError(errC)
}

// SetTitle sets the title of this window.
func (wnd *Window) SetFullscreen(fullscreen bool) {
	if fullscreen {
		C.g2d_window_set_fullscreen(wnd.DataC, 1)
	} else {
		C.g2d_window_set_fullscreen(wnd.DataC, 0)
	}
}

// Destroy releases win32 ressources.
func (wnd *Window) Destroy() error {
	var errC unsafe.Pointer
	if wnd.DataC != nil {
		C.g2d_window_destroy(wnd.DataC, &errC)
	}
	return wnd.toError(errC)
}

// Free releases memory of the C struct that DataC points to.
func (wnd *Window) Free() {
	if wnd.DataC != nil {
		C.g2d_window_data_free(wnd.DataC)
		wnd.DataC = nil
	}
}

// toError converts C error to Go error.
func (wnd *Window) toError(errC unsafe.Pointer) error {
	if errC != nil {
		var errStr string
		var errNumC C.int
		var errWin32 C.g2d_ul_t
		var errStrC *C.char
		C.g2d_window_error(errC, &errNumC, &errWin32, &errStrC)
		C.g2d_window_free(errC)
		switch errNumC {
		case 1:
			errStr = "get module instance failed"
		case 50:
			errStr = "register class failed"
		case 51:
			errStr = "create window failed"
		case 52:
			errStr = "get device context failed"
		case 53:
			errStr = "choose pixel format failed"
		case 54:
			errStr = "set pixel format failed"
		case 55:
			errStr = "create render context failed"
		case 56:
			errStr = "make context current failed"
		case 57:
			errStr = "release context failed"
		case 58:
			errStr = "deleting render context failed"
		case 59:
			errStr = "destroying window failed"
		case 60:
			errStr = "unregister class failed"
		case 61:
			errStr = "swap buffer failed"
		case 62:
			errStr = "set title failed"
		case 63:
			errStr = "context C struct not available"
		case 64:
			errStr = "window C struct not available"
		default:
			errStr = "g2d: unknown error " + strconv.FormatUint(uint64(errNumC), 10)
		}
		if errWin32 != 0 {
			errStr = errStr + " (" + strconv.FormatUint(uint64(errWin32), 10) + ")"
		}
		if errStrC != nil {
			errStr = errStr + "; " + C.GoString(errStrC)
			C.g2d_window_free(unsafe.Pointer(errStrC))
		}
		return errors.New(errStr)
	}
	return nil
}
