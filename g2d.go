/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package g2d creates windows with OpenGL 3.0 context.
package g2d

const (
	notInitStr = "g2d not initialized"
)

var (
	initialized bool
)
