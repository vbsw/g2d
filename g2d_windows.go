/*
 *          Copyright 2022, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package g2d

import (
	"gitlab.com/vbsw/g2d/win32/dummy"
	"gitlab.com/vbsw/g2d/win32/graphics"
	"gitlab.com/vbsw/g2d/win32/instance"
	"unsafe"
)

var (
	inst unsafe.Pointer
)

// Init initializes OpenGL functions.
func Init() error {
	var err error
	if !initialized {
		inst, err = instance.Instance()
		if err == nil {
			dummy := dummy.New()
			err = dummy.Init(inst)
			if err == nil {
				ctx := dummy.Context()
				err = ctx.MakeCurrent()
				if err == nil {
					err = graphics.Init()
				}
				if err == nil {
					err = ctx.Release()
				} else {
					ctx.Release()
				}
				if err == nil {
					err = dummy.Destroy()
				} else {
					dummy.Destroy()
				}
			}
			dummy.Free()
		}
		initialized = bool(err == nil)
	}
	return err
}
