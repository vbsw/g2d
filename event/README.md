# event

[![GoDoc](https://godoc.org/gitlab.com/vbsw/g2d/event?status.svg)](https://godoc.org/gitlab.com/vbsw/g2d/event) [![Stability: Experimental](https://masterminds.github.io/stability/experimental.svg)](https://masterminds.github.io/stability/experimental.html)

## About
event is a package for Go providing events. It is published on <https://gitlab.com/vbsw/g2d/event>.

## Copyright
Copyright 2021, Vitali Baumtrok (vbsw@mailbox.org).

event is distributed under the Boost Software License, version 1.0. (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)

event is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Boost Software License for more details.

## References
- https://golang.org/doc/install
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
