/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package event provides events.
package event

// ButtonDown represents button pressed event.
type ButtonDown struct {
	Button      int
	DoubleClick bool
}

// ButtonUp represents button released event.
type ButtonUp struct {
	Button int
}

// KeyDown represents key pressed event.
type KeyDown struct {
	Key, Repeat int
}

// KeyUp represents key released event.
type KeyUp struct {
	Key int
}

// ClientMove represents window dragging.
type ClientMove struct {
	X, Y int
}

// ClientMoveBegin represents start of window dragging using window manager.
type ClientMoveBegin struct {
}

// ClientMoveEnd represents end of window dragging using window manager.
type ClientMoveEnd struct {
}

// ClientResize represents client resizing.
type ClientResize struct {
	Width, Height int
}

// ClientResizeBegin represents start of client resizing using window manager.
type ClientResizeBegin struct {
}

// ClientResizeEnd represents end of client resizing using window manager.
type ClientResizeEnd struct {
}

// MouseMove represents mouse moving.
type MouseMove struct {
	X, Y int
}

// MouseWheel represents mouse wheel rotating.
type MouseWheel struct {
	Wheel int
}

// FocusLoose represents loosing window focus.
type FocusLoose struct {
}

// FocusGain represents gaining window focus.
type FocusGain struct {
}

// Minimize represents window minimizing.
type Minimize struct {
}

// Restore represents window restoring from minimized state.
type Restore struct {
}

// Update represents window updating the scene.
type Update struct {
}

// Close represents window close request.
type Close struct {
}

// Destroy represents window destroying.
type Destroy struct {
}

// NewButtonDown returns a new instance of ButtonDown.
func NewButtonDown(button int, doubleClick bool) *ButtonDown {
	return &ButtonDown{button, doubleClick}
}

// NewButtonUp returns a new instance of ButtonUp.
func NewButtonUp(button int) *ButtonUp {
	return &ButtonUp{button}
}

// NewKeyDown returns a new instance of KeyDown.
func NewKeyDown(key, repeat int) *KeyDown {
	return &KeyDown{key, repeat}
}

// NewKeyUp returns a new instance of KeyUp.
func NewKeyUp(key int) *KeyUp {
	return &KeyUp{key}
}

// NewClientMove returns a new instance of ClientMove.
func NewClientMove(x, y int) *ClientMove {
	return &ClientMove{x, y}
}

// NewClientMoveBegin returns a new instance of ClientMoveBegin.
func NewClientMoveBegin() *ClientMoveBegin {
	return new(ClientMoveBegin)
}

// NewClientMoveEnd returns a new instance of ClientMoveEnd.
func NewClientMoveEnd() *ClientMoveEnd {
	return new(ClientMoveEnd)
}

// NewClientResize returns a new instance of ClientResize.
func NewClientResize(width, height int) *ClientResize {
	return &ClientResize{width, height}
}

// NewClientResizeBegin returns a new instance of ClientResizeBegin.
func NewClientResizeBegin() *ClientResizeBegin {
	return new(ClientResizeBegin)
}

// NewClientResizeEnd returns a new instance of ClientResizeEnd.
func NewClientResizeEnd() *ClientResizeEnd {
	return new(ClientResizeEnd)
}

// NewMouseMove returns a new instance of MouseMove.
func NewMouseMove(x, y int) *MouseMove {
	return &MouseMove{x, y}
}

// NewMouseWheel returns a new instance of MouseWheel.
func NewMouseWheel(wheel int) *MouseWheel {
	return &MouseWheel{wheel}
}

// NewFocusLoose returns a new instance of FocusLoose.
func NewFocusLoose() *FocusLoose {
	return new(FocusLoose)
}

// NewFocusGain returns a new instance of FocusGain.
func NewFocusGain() *FocusGain {
	return new(FocusGain)
}

// NewMinimize returns a new instance of Minimize.
func NewMinimize() *Minimize {
	return new(Minimize)
}

// NewRestore returns a new instance of Restore.
func NewRestore() *Restore {
	return new(Restore)
}

// NewUpdate returns a new instance of Update.
func NewUpdate() *Update {
	return new(Update)
}

// NewClose returns a new instance of Close.
func NewClose() *Close {
	return new(Close)
}

// NewDestroy returns a new instance of Destroy.
func NewDestroy() *Destroy {
	return new(Destroy)
}
