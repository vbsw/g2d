# queue

[![GoDoc](https://godoc.org/gitlab.com/vbsw/g2d/common/queue?status.svg)](https://godoc.org/gitlab.com/vbsw/g2d/common/queue) [![Stability: Experimental](https://masterminds.github.io/stability/experimental.svg)](https://masterminds.github.io/stability/experimental.html)

## About
queue is a package for Go to create a simple First-In-First-Out queue. It is published on <https://gitlab.com/vbsw/g2d/common/queue>.

## Copyright
Copyright 2022, Vitali Baumtrok (vbsw@mailbox.org).

queue is distributed under the Boost Software License, version 1.0. (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)

queue is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Boost Software License for more details.

## References
- https://golang.org/doc/install
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
