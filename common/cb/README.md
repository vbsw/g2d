# cb

[![GoDoc](https://godoc.org/gitlab.com/vbsw/g2d/common/cb?status.svg)](https://godoc.org/gitlab.com/vbsw/g2d/common/cb) [![Stability: Experimental](https://masterminds.github.io/stability/experimental.svg)](https://masterminds.github.io/stability/experimental.html)

## About
cb is a package for Go to track objects by ids. This allows to manage callbacks from C to Go. It is published on <https://gitlab.com/vbsw/g2d/common/cb>.

## Copyright
Copyright 2022, Vitali Baumtrok (vbsw@mailbox.org).

cb is distributed under the Boost Software License, version 1.0. (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)

cb is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Boost Software License for more details.

## References
- https://golang.org/doc/install
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
