/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

// Package queue provides a First-In-First-Out queue.
package queue

// Queue is a First-In-First-Out buffer.
type Queue struct {
	data       []interface{}
	readIndex  int
	writeIndex int
	empty      bool
}

// New returns a new queue. If capacity is negative,
// capacity is set to zero. If capacity is zero, queue
// is initialized with capacity of 8 at first write.
func New(capacity int) *Queue {
	que := new(Queue)
	if capacity > 0 {
		que.data = make([]interface{}, capacity)
	}
	que.empty = true
	return que
}

// Put appends data at the end of the queue.
func (que *Queue) Put(data interface{}) {
	que.ensureCapacity()
	que.data[que.writeIndex] = data
	que.writeIndex++
	if que.writeIndex == cap(que.data) {
		que.writeIndex = 0
	}
	que.empty = false
}

// PutAll appends data at the end of the queue.
func (que *Queue) PutAll(data ...interface{}) {
	if len(data) > 0 {
		if que.empty {
			que.putAllEmpty(data)
		} else if que.readIndex < que.writeIndex {
			que.putAllOne(data)
		} else {
			que.putAllTwo(data)
		}
	}
}

// putAllEmpty puts data to the empty queue.
func (que *Queue) putAllEmpty(data []interface{}) {
	if len(que.data) < len(data) {
		capNew := len(que.data)
		for capNew < len(data) {
			capNew = 2 * capNew
		}
		que.data = make([]interface{}, capNew)
	}
	copy(que.data, data)
	if len(que.data) > len(data) {
		que.writeIndex = len(data)
	} else {
		que.writeIndex = 0
	}
	que.readIndex = 0
	que.empty = false
}

// putAllOne appends data to the end of the queue.
func (que *Queue) putAllOne(data []interface{}) {
	sizeUsed := que.writeIndex - que.readIndex
	if len(que.data) < sizeUsed+len(data) {
		capNew := len(que.data)
		for capNew < sizeUsed+len(data) {
			capNew = 2 * capNew
		}
		dataNew := make([]interface{}, capNew)
		copy(dataNew, que.data[que.readIndex:que.writeIndex])
		copy(dataNew[sizeUsed:], data)
		que.data = dataNew
		que.readIndex = 0
		if capNew < sizeUsed+len(data) {
			que.writeIndex = sizeUsed + len(data)
		} else {
			que.writeIndex = 0
		}
	} else {
		lenRight := len(que.data) - que.writeIndex
		if lenRight > len(data) {
			copy(que.data[que.writeIndex:], data)
			que.writeIndex = que.writeIndex + len(data)
		} else if lenRight < len(data) {
			copy(que.data[que.writeIndex:], data[:lenRight])
			copy(que.data, data[lenRight:])
			que.writeIndex = len(data) - lenRight
		} else {
			copy(que.data[que.writeIndex:], data)
			que.writeIndex = 0
		}
	}
}

// putAllTwo appends data to the end of the queue.
func (que *Queue) putAllTwo(data []interface{}) {
	sizeUsed := len(que.data) - que.readIndex + que.writeIndex
	if sizeUsed >= len(data) {
		copy(que.data[que.writeIndex:], data)
		que.writeIndex += len(data)
	} else {
		capNew := len(que.data)
		for capNew < sizeUsed+len(data) {
			capNew = 2 * capNew
		}
		dataNew := make([]interface{}, capNew)
		dataRight := que.data[que.readIndex:]
		dataLeft := que.data[:que.writeIndex]
		copy(dataNew, dataRight)
		copy(dataNew[len(dataRight):], dataLeft)
		copy(dataNew[sizeUsed:], data)
		que.readIndex = 0
		que.writeIndex = sizeUsed + len(data)
	}
}

// First retrieves first element from queue.
func (que *Queue) First() interface{} {
	if que.empty {
		return nil
	}
	data := que.data[que.readIndex]
	que.readIndex++
	if que.readIndex == cap(que.data) {
		que.readIndex = 0
	}
	que.empty = bool(que.readIndex == que.writeIndex)
	return data
}

// All retrieves all data from queue.
func (que *Queue) All() []interface{} {
	if que.empty {
		que.readIndex = 0
		que.writeIndex = 0
		return nil
	} else if que.readIndex < que.writeIndex {
		data := make([]interface{}, que.writeIndex-que.readIndex)
		copy(data, que.data[que.readIndex:que.writeIndex])
		que.readIndex = 0
		que.writeIndex = 0
		que.empty = true
		return data
	}
	dataLeft := que.data[:que.writeIndex]
	dataRight := que.data[que.readIndex:]
	data := make([]interface{}, len(dataLeft)+len(dataRight))
	copy(data, dataLeft)
	copy(data[len(dataLeft):], dataRight)
	que.readIndex = 0
	que.writeIndex = 0
	que.empty = true
	return data
}

// Size returns the number of data in queue.
func (que *Queue) Size() int {
	if que.empty {
		return 0
	} else if que.readIndex < que.writeIndex {
		return que.writeIndex - que.readIndex
	}
	return cap(que.data) - que.readIndex + que.writeIndex
}

// ensureCapacity ensures enough capacity for at least one new element in data array.
func (que *Queue) ensureCapacity() {
	if cap(que.data) > 0 {
		if !que.empty && que.writeIndex == que.readIndex {
			dataNew := make([]interface{}, 2*cap(que.data))
			dataRight := que.data[que.readIndex:]
			copy(dataNew, dataRight)
			copy(dataNew[len(dataRight):], que.data)
			que.readIndex = 0
			que.writeIndex = len(que.data)
			que.data = dataNew
		}
	} else {
		que.data = make([]interface{}, 8)
	}
}
