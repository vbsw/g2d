/*
 *          Copyright 2021, Vitali Baumtrok.
 * Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE or copy at
 *        http://www.boost.org/LICENSE_1_0.txt)
 */

package queue

import "testing"

func TestPutEmpty(t *testing.T) {
	q := new(Queue)
	q.empty = true
	q.Put(10)
	if len(q.data) == 0 {
		t.Error("queue not initialized")
	} else if q.readIndex == q.writeIndex {
		t.Error(q.readIndex, q.writeIndex)
	} else {
		q.Put(20)
		if len(q.data) == 0 {
			t.Error("queue not initialized")
		} else if q.readIndex == q.writeIndex {
			t.Error(q.readIndex, q.writeIndex)
		}
	}
}

func TestPutCap(t *testing.T) {
	q := new(Queue)
	q.empty = true
	q.data = make([]interface{}, 1)
	q.Put(10)
	if len(q.data) != 1 {
		t.Error(len(q.data))
	} else if q.readIndex != q.writeIndex {
		t.Error(q.readIndex, q.writeIndex)
	} else if q.empty {
		t.Error("queue is marked empty")
	} else {
		q.Put(20)
		if len(q.data) == 1 {
			t.Error("queue not expanded")
		} else if q.readIndex != q.writeIndex {
			t.Error(q.readIndex, q.writeIndex)
		}
	}
}

func TestSize(t *testing.T) {
	q := new(Queue)
	q.empty = true
	if q.Size() != 0 {
		t.Error(q.Size())
	}
	q.Put(10)
	if q.Size() != 1 {
		t.Error(q.Size())
	}
	q.Put(20)
	if q.Size() != 2 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 1 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 0 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 0 {
		t.Error(q.Size())
	}
}

func TestPutTwo(t *testing.T) {
	q := new(Queue)
	q.data = make([]interface{}, 10)
	q.empty = true
	q.readIndex = 8
	q.writeIndex = 8
	q.Put(10)
	if q.readIndex != 8 {
		t.Error(q.readIndex)
	}
	if q.writeIndex != 9 {
		t.Error(q.writeIndex)
	}
	q.Put(20)
	if q.readIndex != 8 {
		t.Error(q.readIndex)
	}
	if q.writeIndex != 0 {
		t.Error(q.writeIndex)
	}
	q.Put(30)
	if q.readIndex != 8 {
		t.Error(q.readIndex)
	}
	if q.writeIndex != 1 {
		t.Error(q.writeIndex)
	}
}

func TestPutTwoSize(t *testing.T) {
	q := new(Queue)
	q.data = make([]interface{}, 10)
	q.empty = true
	q.readIndex = 8
	q.writeIndex = 8
	if q.Size() != 0 {
		t.Error(q.Size())
	}
	q.Put(10)
	if q.Size() != 1 {
		t.Error(q.Size())
	}
	q.Put(20)
	if q.Size() != 2 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 1 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 0 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 0 {
		t.Error(q.Size())
	}
}

func TestFirst(t *testing.T) {
	q := new(Queue)
	q.data = make([]interface{}, 10)
	q.empty = true
	q.readIndex = 8
	q.writeIndex = 8
	if q.Size() != 0 {
		t.Error(q.Size())
	}
	q.Put(10)
	if q.Size() != 1 {
		t.Error(q.Size())
	}
	q.Put(20)
	if q.Size() != 2 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 1 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 0 {
		t.Error(q.Size())
	}
	q.First()
	if q.Size() != 0 {
		t.Error(q.Size())
	}
}

func TestFirstTwo(t *testing.T) {
	q := new(Queue)
	q.data = make([]interface{}, 10)
	q.empty = true
	q.readIndex = 8
	q.writeIndex = 8
	el := q.First()
	if el != nil {
		t.Error("element not nil")
	}
	arr := []int{10, 20, 30, 40, 50}
	for _, num := range arr {
		q.Put(num)
	}
	for i, num := range arr {
		el = q.First()
		if el == nil {
			t.Error("in", i, "element is nil")
		} else if num2, ok := el.(int); ok && num2 != num {
			t.Error("in", i, "element is not", num)
		}
	}
	el = q.First()
	if el != nil {
		t.Error("element is not nil")
	}
}

func TestPutAll(t *testing.T) {
	q := new(Queue)
	q.data = make([]interface{}, 10)
	q.empty = true
	q.readIndex = 8
	q.writeIndex = 8
	q.PutAll(10, 20, 30)
	if q.readIndex != 0 {
		t.Error(q.readIndex)
	}
	if q.writeIndex != 3 {
		t.Error(q.writeIndex)
	}
	if q.Size() != 3 {
		t.Error(q.Size())
	}
}

func TestAll(t *testing.T) {
	q := new(Queue)
	q.data = make([]interface{}, 10)
	q.empty = true
	q.readIndex = 8
	q.writeIndex = 8
	q.PutAll(10, 20, 30)
	elements := q.All()
	if q.Size() != 0 {
		t.Error(q.Size())
	}
	if len(elements) != 3 {
		t.Error(len(elements))
	}
	for i, el := range elements {
		if el == nil {
			t.Error("in", i, "element is nil")
		} else if num, ok := el.(int); ok && num != el {
			t.Error("in", i, "element is not", el)
		}
	}
}
