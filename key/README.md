# key

[![GoDoc](https://godoc.org/gitlab.com/vbsw/g2d/key?status.svg)](https://godoc.org/gitlab.com/vbsw/g2d/key) [![Stability: Experimental](https://masterminds.github.io/stability/experimental.svg)](https://masterminds.github.io/stability/experimental.html)

## About
key is a package for Go providing keyboard codes as defined in Universal Serial Bus Human Interface Devices. It is published on <https://gitlab.com/vbsw/g2d/key>.

## Copyright
Copyright 2021, Vitali Baumtrok (vbsw@mailbox.org).

key is distributed under the Boost Software License, version 1.0. (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)

key is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Boost Software License for more details.

## Advise
Use dot import to avoid conflict with viriable names.

	import . "gitlab.com/vbsw/g2d/key"

	func foo() {
		...
		if key == KeyA {
			...
		}
	}

## References
- https://golang.org/doc/install
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- https://www.toomanyatoms.com/computer/usb_keyboard_codes.html
- http://ts.thrustmaster.com/download/accessories/pc/hotas/software/TARGET/TARGET_SCRIPT_EDITOR_basics_v1.2_Appendix.pdf
